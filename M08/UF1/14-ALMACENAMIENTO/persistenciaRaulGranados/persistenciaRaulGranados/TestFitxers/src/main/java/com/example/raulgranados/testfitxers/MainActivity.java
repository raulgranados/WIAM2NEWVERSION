package com.example.raulgranados.testfitxers;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {
    private Button btnEscribirFichero = null;
    private Button btnAñadirAFichero = null;
    private Button btnLeerFichero = null;
    private Button btnLeerRaw = null;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEscribirFichero = (Button) findViewById(R.id.BtnEscribirFichero);

        btnLeerFichero = (Button) findViewById(R.id.BtnLeerFichero);

        btnAñadirAFichero = (Button) findViewById(R.id.BtnAñadirAFichero);

        btnLeerRaw = (Button) findViewById(R.id.BtnLeerArchivoEstatico);

        //escribir en fichero
        btnEscribirFichero.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                {
                    try {

                        int limite = 50;

                        EditText editText = (EditText) findViewById(R.id.textoArchivo);
                        String texto = editText.getText().toString();

                        //OutputStreamWriter fout = new OutputStreamWriter(openFileOutput("prueba_int.txt", Context.MODE_PRIVATE));

                        File f = new File("prueba_int.txt");

                        OutputStreamWriter fout = new OutputStreamWriter(openFileOutput(f.getName(), Context.MODE_PRIVATE));

                        //long freeBytesInternal = new File(ctx.getFilesDir().getAbsoluteFile().toString()).getFreeSpace();

                        fout.write(texto);

                        long bytesDisponibles = f.getFreeSpace();

                        if ( bytesDisponibles < (long)limite ) {
                            f.delete();
                        }

                        fout.close();

                        Log.i("Ficheros", "Fichero creado!, queda " + bytesDisponibles + " bytes disponibles en la memoria interna.");

                    } catch (Exception ex) {

                        Log.e("Ficheros", "Error al escribir fichero a memoria interna");
                    }
                }
            }
        });

        //añadir texto en fichero existente
        btnAñadirAFichero.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                {
                    try {

                        EditText editText = (EditText) findViewById(R.id.textoArchivo);
                        String texto = editText.getText().toString();

                        OutputStreamWriter fout = new OutputStreamWriter(openFileOutput("prueba_int.txt", Context.MODE_APPEND));

                        fout.write(texto);
                        fout.close();

                        Log.i("Ficheros", "El texto ha sido añadido!");

                    } catch (Exception ex) {

                        Log.e("Ficheros", "Error al escribir fichero a memoria interna");
                    }
                }
            }
        });

        //leer de fichero
        btnLeerFichero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    BufferedReader fin = new BufferedReader(new InputStreamReader(openFileInput("prueba_int.txt")));

                    String texto = fin.readLine();
                    fin.close();

                    Log.i("Ficheros", "Fichero leido!");
                    Log.i("Ficheros", "Texto: " + texto);
                } catch (IOException ex) {
                    Log.e("Ficheros", "Error al leer el fichero de memoria interna");
                }

            }
        });

        //leer de la carpeta de recursos
        //(previamente la hemos tenido que crear:
        //res -> new -> Android resources Directory -> Resource type -> raw )
        btnLeerRaw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String linea = "";
                try {
                    InputStream fraw =
                            getResources().openRawResource(R.raw.ficheroexistente);

                    BufferedReader brin =
                            new BufferedReader(new InputStreamReader(fraw));

                    linea = brin.readLine();
                    fraw.close();

                    Log.i("Ficheros", "Fichero RAW leido!");
                    Log.i("Ficheros", "Texto: " + linea);
                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde recurso raw");
                }
            }
        });

    }

}
