package jamoros.escoladeltreball.org.sharedpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String KEY_NAME1 = "login";
    private final static String KEY_NAME2 = "password";
    private final static String TOAST_MESSAGE = "Com vosté comprendrà no es pot mostrar la contrasenya de: ";
    private SharedPreferences sharedPrefFile;
    private String sharedPreferencesFileName = "dades";
    private TextView tvName;
    private TextView tvPassword;
    private Button updateButton;
    private String currentName;
    private String currentPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvName = (TextView) findViewById(R.id.editTextName);
        tvPassword = (TextView) findViewById(R.id.editTextPassword);
        updateButton = (Button) findViewById(R.id.button);
        updateButton.setOnClickListener(this);

        // Recuperem les dades suposant que el fitxer és únic
        //sharedPrefFile = this.getPreferences(MODE_PRIVATE);

        // Recuperem les dades suposant que el fitxer NO és únic
        sharedPrefFile = this.getSharedPreferences(sharedPreferencesFileName,MODE_PRIVATE);


        currentName = sharedPrefFile.getString(KEY_NAME1, "Papitu");
        currentPassword = sharedPrefFile.getString(KEY_NAME2, "Secret");

        Toast.makeText(this, TOAST_MESSAGE + currentName, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Però entre tu i jo és: " + currentPassword, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        currentName = tvName.getText().toString();
        currentPassword = tvPassword.getText().toString();
    }

    @Override
    protected void onPause() {
        SharedPreferences.Editor editor = sharedPrefFile.edit();
        editor.putString(KEY_NAME1, currentName);
        editor.putString(KEY_NAME2, currentPassword);

        // Ull viu! Alerta! si ens oblidem de la següent ordre no s'actualitzarà
        // el fitxer.
        //editor.commit();
        editor.apply();  // (*)

        super.onPause();
    }
}

/*
    (*) Podem utilitzar tant commit() com apply(). Simplificant l'elecció podríem pensar que si fem servir API
    anteriors a la 9, hem de fer servir commit() perquè apply encara no existia. Si volem un valor de retorn per
    controlar si ha anat be el proces o no, també hauríem d'utilitzar commit (apply() és void).
    Però en la resta de casos, és a dir, la gran majoria farem servir apply.
    Una altra diferència important és que commit és una tasca síncrona (fins que no acabi hi ha bloqueig) mentre que
    apply no. Més info:
    https://nayaneshguptetechstuff.wordpress.com/2014/07/09/sharedpreferences-apply-vs-commit/
 */
