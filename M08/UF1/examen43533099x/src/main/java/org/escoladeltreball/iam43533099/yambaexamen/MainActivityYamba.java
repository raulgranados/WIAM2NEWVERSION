package org.escoladeltreball.iam43533099.yambaexamen;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

public class MainActivityYamba extends Activity implements View.OnClickListener {

    //constant que utilitzarem al mètode Log.d( , )

    private static final String TAG = "StatusActivity";
    private String oldMessage = "Esperem que l'usuari no possi aquest mateix" +
            " missatge al principi o l'app no funcionarà";

    // Declaramos  nuevas variables para checkear el login del usuario

    String user; // Guardaremos el contenido del EditText login;
    String password; // Guardaremos el contenido del EditText password;
    EditText editTextLogin; // Obtendremos los views login y password;
    EditText editTextPassword;

    Twitter twitter;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Cridem al mètode de la superclasse
        super.onCreate(savedInstanceState);

        // A partir del layout manager definit en codi XML  generem els
        // objecte Java adients, d'això se'n diu “inflate XML”
        setContentView(R.layout.activity_main_activity_yamba);

        // Trobem les Views que utilitzarem a partir del seu id

        editTextLogin = (EditText) findViewById(R.id.login_text);
        editTextPassword = (EditText) findViewById(R.id.pasword_text);
        Button button = (Button) findViewById(R.id.buttonUpdate);
        editText = (EditText) findViewById(R.id.message_text);

        // Recuperamos el contenido de los views y lo asignamos a las
        // respectivas variables;

        String user = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();

        // Registrem el botó per a notificar quan és clickat

        button.setOnClickListener(this);

        // Ens connectem al servei online que suporta l'API de Yamba (un lloc «tipus» Twitter que utilitza Status.net)
        // No ens fixem que hem posat hardcoded l'usuari i el password

        twitter = new Twitter("student", "password");

        //ja no funciona la url antiga de marakana, ja que Twitter ha comprat el domini i l'ha canviat
        //twitter.setAPIRootUrl("http://yamba.marakana.com/api");

        twitter.setAPIRootUrl("http://yamba.newcircle.com/api");
    }

    // Es crida quan el botó es premut

    @Override
    public void onClick(View v) {

        // Capturem el missatge

        String message = editText.getText().toString();

        //Fem que l'API del servei web actualitzi el nostre status a Yamba

        twitter.setStatus(message);

        // Creamos un objeto de la clase AsyncTask y llamamos a su método
        // execute() para que realize la tarea en segundo plano
        // (la pasamos por parametros, el mensaje, el login(user) y el password)

        EjemploAsyncTask tarea2 = new EjemploAsyncTask();
        tarea2.execute(message,user,password);

    }

    // Creamos la clase AsynTask para realizar la tarea en segundo plano, recibira como parametros
    // el mensaje, el login(user) y el password, que recibe y necesita el metodo doInBackground(),
    // un void ya que no analizaremos el progreso de la tarea y
    // por último, otro void, ya que nuestro método no devuelve nada.

    private class EjemploAsyncTask extends AsyncTask<String,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            /*runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivityYamba.this, "Estamos en el método onPreExecute()",
                            Toast.LENGTH_SHORT).show();
                }
            });*/
        }

        @Override
        protected Void doInBackground(String... params) {

            final String message =  params[0];
            String user = params[1];
            String password = params[2];

            // Comprobamos si la cadena es nula o si son
            // espacios, en tal caso mostramos mensaje informativo

            try {

            // Si la cadena, el login o el passwords son buits no actualitzis i mostra missatge

            if (message.equals("") || user.trim().equals("") ||  password.trim().equals(("")) ) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivityYamba.this, "No pot ser la cadena buida",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (message.equals(oldMessage)) { // Si repeteixes missatge, no actualitzis i mostra missatge
                //  (*)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivityYamba.this, "No es pot repetir la cadena",
                                Toast.LENGTH_SHORT).show();
                    }
                });

            } else {

                // Creamos el objeto calendar para obtener el tiempo y la hora
                Calendar cal = new GregorianCalendar(TimeZone.getDefault());
                DateFormat date_format = DateFormat.getDateInstance(DateFormat.DEFAULT);

                // Añadimos la fecha y hora al mensaje
                twitter.setStatus(message+ "("+date_format.format(cal.getTime())+")");
                oldMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivityYamba.this, message,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }


                // (*)
                // Llamamos al método runOnUiThread() para “enviar” operaciones al hilo principal desde el hilo secundario
                // Le pasamos como parámetro un objeto Runnable
                // Implementamos su método run()

        }catch (TwitterException e){
            e.printStackTrace(); // controlamos si se produce algun tipo de error.
        }
            // Fem un log cada cop que clickem el botó (podríem diferenciar si la
            // cadena es buida o no)

            Log.d(TAG, "onClicked");

            return null;
        }

    }
}
