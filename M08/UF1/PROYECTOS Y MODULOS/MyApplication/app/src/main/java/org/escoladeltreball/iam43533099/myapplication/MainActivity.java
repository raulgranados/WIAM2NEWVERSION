package org.escoladeltreball.iam43533099.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Remember <Ctrl> + </> to comment the selected lines
        // If that lines were already comented you will uncomment the lines

        // I want to show a message using Toast class when onCreate method is running
        Toast.makeText(this, "Activity A: estic executant onCreate!", Toast.LENGTH_LONG).show();

        setContentView(R.layout.activity_main);
    }

    /*
     * I want to show a message using Toast class when onStart method is running
     * (non-Javadoc)
     * @see android.app.Activity#onStart()
     */

    @Override
    protected void onStart() {

        Toast.makeText(this, "Activity A: estic executant onStart!", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onResume() {
        Toast.makeText(this, "Activity A: estic executant onResume!", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onPause() {
        Toast.makeText(this, "Activity A: estic executant onPause!", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onStop() {
        Toast.makeText(this, "Activity A: estic executant onStop!", Toast.LENGTH_LONG)
                .show();
    }
}