package org.escoladeltreball.jamoros.threadingintrojava;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* Thread helloThread = new Thread(new HelloLoopRunnable());
        helloThread.start();
*/

	/*
         (new Thread(new HelloLoopRunnable())).start();
	*/

        (new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                for (int i = 0; i < 1000; i++) {
                    System.out.println("Hello " + i + " from a thread!");
                }
            }

        })
        ).start();




        for (int i = 0; i < 1000; i++) {
            System.out.println("Goodbye " + i + " from the main thread!");
        }

    }
}

