package com.example.raul.conversordetemperatura;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Recuperamos las views boton_f y boton_c, a partir de sus respectivos id's

        Button buttonf = (Button) findViewById(R.id.boton_f); //
        buttonf.setOnClickListener(this);

        Button buttonc = (Button) findViewById(R.id.boton_c);
        buttonc.setOnClickListener(this);

    }

    // Metodo al que se llama cuando clickamos sobre una vista

    @Override
    public void onClick(View v) {

        // mediante el metodo getId() obtenemos el id del view  ue ha llamado al metodo

        // Si se ha clickado toFahrenheit realizamos el siguiente bloque de código:

        if (v.getId() == R.id.boton_f) {

            EditText text = (EditText) findViewById(R.id.celsius);

            if (text.length()==0) {
                Toast.makeText(MainActivity.this, "No pot ser la cadena buida",
                        Toast.LENGTH_SHORT).show();
            } else {

                Double enteredCelsius = Double.parseDouble(text.getText().toString());

                EditText resultadoFahrenheit = (EditText) findViewById(R.id.fahrenheit);

                enteredCelsius = (enteredCelsius - 32) / 1.8000;
                resultadoFahrenheit.setText(enteredCelsius.toString());

            }
        // sino, es decir, se ha clickado toCelsius, realizamos:

        } else {

            EditText text = (EditText) findViewById(R.id.fahrenheit);

            if (text.length()==0) {
                Toast.makeText(MainActivity.this, "No pot ser la cadena buida",
                        Toast.LENGTH_SHORT).show();
            } else {

                Double enteredFahrenheit = Double.parseDouble(text.getText().toString());

                EditText resultadoCelsius = (EditText) findViewById(R.id.celsius);

                enteredFahrenheit = (enteredFahrenheit - 32) / 1.8000;
                resultadoCelsius.setText(enteredFahrenheit.toString());
            }
        }
    }
}
