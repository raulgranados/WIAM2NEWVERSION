package org.escoladeltreball.jamoros.timepicker;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TimePicker
        .OnTimeChangedListener {
    TimePicker timePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // After previous inflate we can reference some view objects we need to use
        Button button = (Button) findViewById(R.id.btnSet);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        // Associate our activity, as a listener of click events, with the button
        button.setOnClickListener(this);

        timePicker.setOnTimeChangedListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View view) {
        /*
            As the only view who was registered for a onClick event was the button,
            there is no doubt about which view has been clicked
         */

        NumberFormat formatter = new DecimalFormat("00");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Toast.makeText(this, "Before reset API23: " + formatter.format(timePicker.getHour())
                    + ":" + formatter.format(timePicker.getMinute()), Toast.LENGTH_SHORT)
                    .show();

            Log.i("HOUR", "" + timePicker.getHour());
            // Due to a poltergeist, at least for me, we have to invent a hack:
            // setting time twice is needed and the time values set have to be different


            // If we comment the next line it doesn't work
            timePicker.setHour(11); // fake hour
            Log.i("HOUR", "" + timePicker.getHour());


            timePicker.setHour(0); // real hour wanted
            Log.i("HOUR", "" + timePicker.getHour());


            timePicker.setMinute(23); // fake minute
            timePicker.setMinute(0); // real minute wanted
        } else {
            Toast.makeText(this, "Before reset: " + formatter.format(timePicker.getCurrentHour())
                    + ":" + formatter.format(timePicker.getCurrentMinute()), Toast.LENGTH_SHORT)
                    .show();

            Log.i("HOUR", "" + timePicker.getCurrentHour());

            // Due to a poltergeist, at least for me, we have to invent a hack:
            // setting time twice is needed and the time values set have to be different
            timePicker.setCurrentHour(23); // fake hour
            Log.i("HOUR", "" + timePicker.getCurrentHour());

            timePicker.setCurrentHour(0);  // real hour wanted
            Log.i("HOUR", "" + timePicker.getCurrentHour());


            timePicker. setCurrentMinute(59); // fake minute
            timePicker.setCurrentMinute(0);  // real minute wanted
        }
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        NumberFormat formatter = new DecimalFormat("00");

        Toast.makeText(this, "Time changed: " + formatter.format(hourOfDay) + ":" + formatter
                .format(minute), Toast.LENGTH_SHORT).show();
    }
}


/*
 The double set hack, setCurrentHour & setCurrentMinute, is not needed when we use the spinner mode
 for the TimePicker, take a look at the layout file (new attribute in API 21 for TimePicker )
 On  the other hand if our target sdk is 22, the setCurrentHour is not deprecated and our "if" is not needed
 */