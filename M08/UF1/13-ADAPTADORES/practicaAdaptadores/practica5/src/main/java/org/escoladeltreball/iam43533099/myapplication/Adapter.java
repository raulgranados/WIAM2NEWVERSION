package org.escoladeltreball.iam43533099.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by iam43533099 on 1/12/16.
 */
public  class Adapter extends BaseAdapter {

    ArrayList <Movie> myList= new ArrayList<Movie>();
    LayoutInflater inflater;

    public Adapter(Context context) {
        this.inflater = inflater.from(context);
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public  void add(Movie pelicula){
        myList.add(pelicula);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if ( convertView==null) {
            convertView = inflater.inflate(R.layout.list_linear_layout, parent, false);
        }

        Movie mv = myList.get(position);

        TextView titol = (TextView) convertView.findViewById(R.id.titol);
        TextView any = (TextView) convertView.findViewById(R.id.any);
        TextView posicio = (TextView) convertView.findViewById(R.id.id);

        titol.setText(mv.getTitol());
        any.setText(mv.getAny());
        posicio.setText("" + mv.getPosicio());

        return convertView;


    }
}
