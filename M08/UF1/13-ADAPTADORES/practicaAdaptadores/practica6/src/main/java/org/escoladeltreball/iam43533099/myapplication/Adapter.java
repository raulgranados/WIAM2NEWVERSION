package org.escoladeltreball.iam43533099.myapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by iam43533099 on 1/12/16.
 */
public  class Adapter extends BaseAdapter {

    ArrayList <Movie> myList= new ArrayList<Movie>();
    LayoutInflater inflater;

    public Adapter(Context context) {
        this.inflater = inflater.from(context);
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public  void add(Movie pelicula){
        myList.add(pelicula);
    }


    private static class ViewHolder {
        public final TextView titolView;
        public final TextView anyView;
        public final TextView posicioView;


        public ViewHolder(TextView titolView, TextView anyView, TextView posicioView) {
            this.titolView = titolView;
            this.anyView = anyView;
            this.posicioView = posicioView;
        }
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView titol;
        TextView any;
        TextView posicio;

        if ( convertView==null) {
            convertView = inflater.inflate(R.layout.list_linear_layout, parent, false);
            titol = (TextView) convertView.findViewById(R.id.titol);
            any = (TextView) convertView.findViewById(R.id.any);
            posicio = (TextView) convertView.findViewById(R.id.id);
            convertView.setTag(new ViewHolder(titol, any, posicio));

        }else {

            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            titol = viewHolder.titolView;
            any = viewHolder.anyView;
            posicio = viewHolder.posicioView;
        }

        Movie mv = myList.get(position);



        titol.setText(mv.getTitol());
        any.setText(mv.getAny());
        posicio.setText("" + mv.getPosicio());

        return convertView;


    }
}
