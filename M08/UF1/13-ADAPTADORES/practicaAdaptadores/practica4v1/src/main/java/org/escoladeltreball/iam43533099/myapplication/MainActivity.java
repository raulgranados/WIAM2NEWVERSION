package org.escoladeltreball.iam43533099.myapplication;

import android.app.ListActivity;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Resources res = getResources();

        String[] movies = res.getStringArray(R.array.movie_title);

        String[] anys = res.getStringArray(R.array.movie_year);

        Adapter adapter1 = new Adapter(getApplicationContext());

        setListAdapter(adapter1);

        for (int i = 0; i < movies.length; i++) {
            Movie mv = new Movie(movies[i], anys[i], i);
            adapter1.add(mv);
        }

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
    }

}
