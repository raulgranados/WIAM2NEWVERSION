package org.escoladeltreball.timepickerno;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	TimePicker timePicker;
	Button button;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		button = (Button) findViewById(R.id.btnSet);
		timePicker = (TimePicker) findViewById(R.id.timePicker);

		timePicker.setIs24HourView(true);

		button.setOnClickListener(this);
		timePicker.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		NumberFormat formatter = new DecimalFormat("00");
		if (v instanceof Button) {
			Toast.makeText(
					this,
					"Time before reset:" + timePicker.getCurrentHour() + ":"
							+ formatter.format(timePicker.getCurrentMinute()),
					Toast.LENGTH_SHORT).show();
			timePicker.setCurrentHour(0);
			timePicker.setCurrentMinute(0);
		} else {
			Toast.makeText(
					this,
					"Time changed: "
							+ formatter.format(timePicker.getCurrentHour())
							+ ":"
							+ formatter.format(timePicker.getCurrentMinute()),
					Toast.LENGTH_SHORT).show();
		}
	}

}
