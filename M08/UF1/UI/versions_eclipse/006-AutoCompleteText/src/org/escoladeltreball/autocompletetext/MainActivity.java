package org.escoladeltreball.autocompletetext;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

//		Aquesta seria una manera no-android de carregar aquesta info: millor a un fitxer xml
//
//		String[] filosofs = { "Karl Marx", "Platón", "Sigmund Freud",
//				"Friedrich Nietzsche", "Friedrich Engels", "Benedetto Croce",
//				"Jean Paul Sartre", "Ernst Cassirer",
//				"Georg Wilhelm Friedrich Hegel", "Jürgen Habermas",
//				"Thomas Hobbes", "Michel Foucault", "Mahatma Gandhi",
//				"Martin Heidegger", "Confucio", "Noam Chomsky",
//				"Jean-Jacques Rousseau", "Henry Adams", "Ludwig Wittgenstein",
//				"Rudolf Carnap", "Ramon Llull", "Friedrich Schiller",
//				"Aristóteles", "Immanuel Kant", "Mencio", "Jacques Lacan",
//				"John Stuart Mill", "Karl Popper", "Thomas Kuhn", "Socrates",
//				"John Locke", "Anthony Giddens", "Francis Bacon",
//				"Arthur Schopenhauer", "René Descartes", "Thomas Paine",
//				"Benjamin Franklin", "Erich Fromm", "George Santayana",
//				"Howard Gardner", "David Hume", "José Ortega y Gasset",
//				"William James", "Roger Bacon", "Peter Singer", "Ayn Rand",
//				"Adam Smith", "Mikhail Bakhtin", "Carl Jung", "Jeremy Bentham",
//				"Giordano Bruno", "Edmund Husserl", "Bertrand Russell",
//				"Howard Zinn", "Carl Gustav Jung", "Wilhelm Wundt",
//				"Jean Piaget", "Hans-Georg Gadamer", "Timothy Leary",
//				"Erasmo de Rotterdam", "Gilles DeLeuze", "Rabindranath Tagore",
//				"Margaret Mead", "Herbert Spencer", "Henri Poincaré",
//				"Georg Simmel", "Voltaire", "Gottlob Frege",
//				"Pierre de Coubertin", "Claude Bernard", "Jules Michelet",
//				"John Dewey", "Averroes", "Edward Gibbon", "Manuel Castells",
//				"John Rawls", "Max Scheler", "Johann Gottfried Herder",
//				"Herbert Marcuse", "Samuel Taylor Coleridge", "Hannah Arendt",
//				"Thomas Carlyle", "Paulo Freire", "Blaise Pascal",
//				"Baruch Spinoza", "Fernand Braudel", "Paul Tillich",
//				"Simone de Beauvoir", "Friedrich Schleiermacher",
//				"Denis Diderot", "Gilberto Freyre", "Alfred North Whitehead",
//				"Umberto Eco", "Robert Boyle", "Niall Ferguson",
//				"Claude Lévi-Strauss", "Melanie Klein", "Ernst Bloch",
//				"Jacques Derrida", "Francisco Suárez" };

		
		// Recupero els filòsofs del fitxer xml

		
		
		// Recuperem una referència del AutoCompleteTextView

		// Creem l'adaptador

		
		// Especifica el mínim número de caràcters que hem d'escriure absn de que es mostri la llista d'opcions
		// Esbrineu que passa si no ho poseu

		
		//endollem l'adaptador a la view


	}

}
