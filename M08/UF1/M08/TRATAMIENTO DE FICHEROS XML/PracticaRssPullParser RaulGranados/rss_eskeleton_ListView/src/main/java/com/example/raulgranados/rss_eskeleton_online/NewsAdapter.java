package com.example.raulgranados.rss_eskeleton_online;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by raulgranados on 7/2/16.
 */
public class NewsAdapter extends ArrayAdapter<New>  {
    private List<New> listaDeNoticias;
    private Context context;

    public NewsAdapter(List<New> listaDeNoticias, Context context) {
        super(context, R.layout.listitem_new,listaDeNoticias);
        this.listaDeNoticias=listaDeNoticias;
        this.context=context;
    }

    public View getView (int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            // Aquesta es la nova view que inflem amb el nou layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listitem_new, parent, false);
        }

        TextView tvTitulo = (TextView) convertView.findViewById(R.id.LblTitulo);
        TextView tvFecha = (TextView) convertView.findViewById(R.id.LblFecha);

        String titulo = listaDeNoticias.get(position).getTitle() + " ";
        String fecha = listaDeNoticias.get(position).getDate();

        tvTitulo.setText(titulo);
        tvFecha.setText(fecha);

        return convertView;

    }
}
