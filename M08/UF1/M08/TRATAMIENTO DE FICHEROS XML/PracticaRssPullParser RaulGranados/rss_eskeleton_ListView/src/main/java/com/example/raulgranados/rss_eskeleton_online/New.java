package com.example.raulgranados.rss_eskeleton_online;

/**
 * Created by raulgranados on 6/2/16.
 */
public class New {
    private String title;
    private String link;
    private String description;
    private String guid;
    private String date;

    public New() {
        this.title = title;
        this.link = link;
        this.description = description;
        this.guid = guid;
        this.date = date;
    }

    public New(String title, String date) {
        this.title = title;
        this.date = date;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
