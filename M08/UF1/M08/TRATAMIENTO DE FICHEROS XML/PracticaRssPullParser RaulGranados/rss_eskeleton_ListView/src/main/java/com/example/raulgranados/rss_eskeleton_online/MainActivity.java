package com.example.raulgranados.rss_eskeleton_online;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button loadButton;
    private List<New> news;
    private String rssLink;
    private ListView listaNoticias;

    final String[] newspapers = new String[]{"MundoToday","Mondediplo","EuropaPress","Ara"};
    private Spinner cmbOpciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflamos el layout
        setContentView(R.layout.activity_main);

        // Recuperamos los objetos inflados
        loadButton = (Button) findViewById(R.id.btnLoad);

        // Registramos el boton load
        loadButton.setOnClickListener(this);

        // Creamos el adaptador para el spinner
        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, newspapers);

        //Obtenemos una referencia al controlador a través de su ID.
        cmbOpciones = (Spinner)findViewById(R.id.CmbOpciones);

        //Utilizamos un layout predefinido para visualizar la lista emergente 
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //Asignamos el adaptador al control mediante el método setAdapter()
        cmbOpciones.setAdapter(adaptador);

        cmbOpciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, android.view.View v, int position, long id) {

                switch ((String) parent.getItemAtPosition(position)) {

                    case "MundoToday":
                        rssLink = "http://www.elmundotoday.com/feed/";
                        break;

                    case "Mondediplo":
                        rssLink = "http://mondediplo.com/backend";
                        break;

                    case "EuropaPress":
                        rssLink = "http://www.europapress.es/rss/rss.aspx";
                        break;

                    case "Ara":
                        rssLink = "http://www.ara.cat/rss/";
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {

                //nothing to do
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        // Amb tasca asíncrona

        LoadXmlTask task = new LoadXmlTask();
        task.execute(rssLink);
    }

    // Tasca asincrona per carregar el fitxer XML en segon pla (background)
    private class LoadXmlTask extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {
            RssPullParser rssParser = new RssPullParser(params[0]) {
            };
            news = rssParser.parse();
            return true;
        }

        protected void onPostExecute(Boolean result) {

            // Tractem la llista de notícies
            listaNoticias = (ListView) findViewById(R.id.list);

            NewsAdapter  adaptadorNoticias= new NewsAdapter(news, MainActivity.this);

            listaNoticias.setTextFilterEnabled(true);

            listaNoticias.setAdapter(adaptadorNoticias);

        }
    }
}