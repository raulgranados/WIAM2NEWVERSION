package org.escoladeltreball.jamoros.buttons;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Recordem que per manegar els esdeveniments del botó 3,
        // necessitem tenir una referència del botó 3 i registrar un escoltador
        // dels esdeveniments adients, "clicks" per exemple,
        // això últim es fa amb el mètode setOnClickListener

        Button btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(this); // Aquesta classe que instanciarem,this:
        // MainActivity, implementa la interfície OnClickListener
        // per tant la puc fer servir com a escoltadora de "onclicks"
        // sobre el botó 3

        Button btn4 = (Button) findViewById(R.id.button4);

        // Registrem l'escoltador amb una clase interna anònima que definim:
        btn4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        MainActivity.this,
                        ((Button) view).getText()
                                + " was clicked using an anonymous class !",
                        Toast.LENGTH_SHORT).show();
            }
        });


        Button btn5 = (Button) findViewById(R.id.button5);
        Button btn6 = (Button) findViewById(R.id.button6);

        btn5.setOnClickListener(btnListener);
        btn6.setOnClickListener(btnListener);

        ToggleButton tb = (ToggleButton) findViewById(R.id.toggleButton);
        tb.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        Button btn = (Button) v;
        if (!(btn instanceof ToggleButton)) {
            // No Togglebutton, o sigui estaríem al cas del botó 3
            Toast.makeText(this, btn.getText() + " was clicked using an Activity class " +
                    "Listener!", Toast.LENGTH_SHORT).show();
        } else if (((ToggleButton) btn).isChecked()) {

            Toast.makeText(this, "I'M ON !", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "I'M OFF !", Toast.LENGTH_SHORT).show();
        }

//         De fet si no és per executar accions ben diferenciades,
//         no cal discriminar d'aquesta manera seria suficient amb:
/*
        if (!(btn instanceof ToggleButton)) {
			// Es a dir estaríem al cas del botó 3
			Toast.makeText(
					this,
					btn.getText()
							+ " was clicked using an Activity class Listener!",
					Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, btn.getText(), Toast.LENGTH_SHORT).show();
		}
*/

    }


    	/*
     * Aquesta és una de les maneres de relacionar l'esdeveniment (en aquest cas
	 * un click de ratolí) sobre una view (en aquest cas un Button) amb l'acció
	 * conseqüència que esdevindrà després
	 */

    public void myClickHandler(View view) {

        // Afegeix codi NOMÉS AQUÍ de manera que si clickem el primer botó afegeixi el
        // toast "Welcome to the Buttons Show"

        Button btn = (Button) view;

        if (btn.getId() == R.id.button1) {
            Toast.makeText(this, "Welcome to the Buttons Show",
                    Toast.LENGTH_SHORT).show();
        }


        Toast.makeText(this, btn.getText() + " was clicked!",
                Toast.LENGTH_SHORT).show();
    }


    // Crearem una variable de tipus interfície al qual li assignem un objecte (una classe interna)
    // que implementa OnclickListener (escoltadora de clicks de botons)
    // Al tenir una referència apuntant a la classe anònima, la podem reutilitzar per altres botons

    private OnClickListener btnListener = new OnClickListener() {

        public void onClick(View view) {
            Toast.makeText(getBaseContext(), ((Button) view).getText() + " was clicked using a " +
                    "reference to an anonymous class !", Toast.LENGTH_LONG).show();
        }
    };
}
