package org.escoladeltreball.jamoros.radiobuttonv2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioButton[] radioButton = new RadioButton[3];
    private Button resetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resetButton = (Button) findViewById(R.id.buttonReset);

        radioButton[0] = (RadioButton) findViewById(R.id.radioButton1);
        radioButton[1] = (RadioButton) findViewById(R.id.radioButton2);
        radioButton[2] = (RadioButton) findViewById(R.id.radioButton3);


        for (int i = 0; i < radioButton.length; i++) {
            radioButton[i].setOnClickListener(this);
        }

        resetButton.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (!(view instanceof RadioButton)) {
            // Then the user has clicked the button
            clearCheck();
        } else {
            checkRadioButton(view);
        }
    }


    private void clearCheck() {
        for (int i = 0; i < radioButton.length; i++) {
            radioButton[i].setChecked(false);
        }
    }


    private void checkRadioButton(View view) {
        // When we find the radiobutton clicked we exit from the wile loop
        int i = 0;
        while (radioButton[i] != view) {
            i++;
        }
        // The click action set the radio button checked (so we don't need to explicitly check)
//        radioButton[i].setChecked(true);

        // We unchecked the rest ones
        for (int j = 1; j < radioButton.length; j++) {
            radioButton[(i + j) % radioButton.length].setChecked(false);
        }
    }

}

/*
    We are using the idea about "if a radio button checked is click nothing happens, is not
    become unchecked". Wit the checkboxes would be different
 */