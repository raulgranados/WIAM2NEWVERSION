package org.escoladeltreball.jamoros.imagebuttons;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton imgBtn = (ImageButton) findViewById(R.id.imageButton);
        imgBtn.setOnClickListener(this);


        Button btn1 = (Button) findViewById(R.id.imageTextButton1);
        Button btn2 = (Button) findViewById(R.id.imageTextButton2);
        Button btn3 = (Button) findViewById(R.id.imageTextButton3);
        Button btn4 = (Button) findViewById(R.id.imageTextButton4);
        Button btn5 = (Button) findViewById(R.id.imageTextButton5);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v instanceof ImageButton) {
            Toast.makeText(this, "I'm an ImageButton", Toast.LENGTH_SHORT)
                    .show();

        } else if (v.getId() == R.id.imageTextButton5) { // se suposa que ara
            // podem dir que és un Button i no un ImageButton
            Toast.makeText(this, "I'm a Vettel", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Well ... it's a bad joke, definitely bad",
                    Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "I'm a Button, too", Toast.LENGTH_SHORT)
                    .show();
        } else {
            Toast.makeText(this, "I'm a Button", Toast.LENGTH_SHORT).show();
        }

    }
}
/*
 * Tant ImageButton com Button tenen coses en comú i pot haver casos en que fer
 * servir un o altre pot ser indiferent, però n'hi ha de diferències. Totes dues
 * deriven de View, però mentre que una ho fa de TextView l'altre ho fa de
 * ImageView. I per exemple, entre d'altres diferències, a un Button es pot
 * posar text, mentre que a un ImageButton no.
 */