package org.escoladeltreball.jamoros.temperatureconverterv0;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TemperatureConverterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etCelsius;
    private EditText etFahrenheit;
    private Button btCelsius;
    private Button btFahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_converter);

        // Recuperem els widgets
        etCelsius = (EditText) findViewById(R.id.celsius_temp);
        etFahrenheit = (EditText) findViewById(R.id.fahrenheit_temp);
        btCelsius = (Button) findViewById(R.id.toCelsiusButton);
        btFahrenheit = (Button) findViewById(R.id.toFahrenheitButton);

        // Registrem els botons, dient que la nostra activitat
        // serà un escoltador de clicks sobre els nostres butons
        btCelsius.setOnClickListener(this);
        btFahrenheit.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_temperature_converter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Button b = (Button) v;

        // Si el botó que obtenim, i per tant ha estat premut, és el de fahrenheit
        if (b == btFahrenheit) {
            // We try to parse for applying the formula later
            // If it s not possible to parse, a NumberFormatException will be thrown
            try {
                double celsius = Double.parseDouble(etCelsius.getText()
                        .toString());
                double fahrenheit = celsius * 9.0 / 5.0 + 32;
                etFahrenheit.setText(String.valueOf(fahrenheit));
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Celsius invàlid", Toast.LENGTH_SHORT)
                        .show();
            }
        } else {
            try {
                double fahrenheit = Double.parseDouble(etFahrenheit.getText()
                        .toString());
                double celsius = (fahrenheit - 32.0) * 5.0 / 9.0;
                etCelsius.setText(String.valueOf(celsius));
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Fahrenheit invàlid", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
