package com.example.raul.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


import static com.example.raul.myapplication.R.id.text_id_name;

public class Main22Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main22);

        TextView newtext = (TextView) findViewById(text_id_name);

        long ahora = System.currentTimeMillis();
        Date fecha = new Date(ahora);

        DateFormat df = new SimpleDateFormat("dd/mm/yy");
        String salida = df.format(fecha);
        salida = getResources().getString(R.string.name);

        newtext.setText(salida);
    }

        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }


    }


