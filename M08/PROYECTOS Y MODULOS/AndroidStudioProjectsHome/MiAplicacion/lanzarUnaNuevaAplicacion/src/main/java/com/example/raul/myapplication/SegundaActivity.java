package com.example.raul.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class SegundaActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_segunda);

        //cada actividad es invocada por un Intent, por tanto, debemos recuperar el intent
        // de la clase MainActivity que lanzo esta nueva actividad

        Intent intent = getIntent();
        String mensaje = intent.getStringExtra(MainActivityLanzarUnaNuevaAplicacion.EXTRA_MESSAGE);

        //por ultimo, para mostrar el mensaje
        //creamos un TextView, modificamos su tamaño y añadimos el mensaje

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(mensaje);


        //set the text view as the activity layout
        setContentView(textView);
    }

}
