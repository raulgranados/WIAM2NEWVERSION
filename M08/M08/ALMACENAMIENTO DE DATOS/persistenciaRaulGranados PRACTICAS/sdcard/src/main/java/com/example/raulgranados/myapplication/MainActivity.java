package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {
    private Button btnLeerArchivoRaw = null;
    private Button btnLeerFicheroSd = null;
    private Button btnEscribirFichero = null;
    private Button btnEscribirFicheroSD = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLeerArchivoRaw = (Button) findViewById(R.id.BtnLeerArchivoRaw);
        btnLeerFicheroSd = (Button) findViewById(R.id.BtnLeerFicheroSd);
        btnEscribirFichero = (Button) findViewById(R.id.BtnEscribirFichero);
        btnEscribirFicheroSD = (Button) findViewById(R.id.BtnEscribirFicheroSd);


        EditText editTextNombre = (EditText) findViewById(R.id.nombreArchivoInternalStorage);
        final String nombre = editTextNombre.getText().toString();

        EditText editTextContenido = (EditText) findViewById(R.id.textoArchivoInternalStorage);
        final String contenido = editTextNombre.getText().toString();


        btnLeerArchivoRaw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String linea = "";
                try {
                    InputStream fraw =
                            getResources().openRawResource(
                                    R.raw.ficheropruebaraw);

                    BufferedReader brin =
                            new BufferedReader(new InputStreamReader(fraw));

                    while ((linea = brin.readLine()) != null) {
                        Log.i("Ficheros", "Texto: " + linea);
                    }
                    fraw.close();

                    Log.i("Ficheros", "Fichero RAW leido!");

                } catch (Exception ex) {
                    Log.e("Ficheros", "Error al leer fichero desde recurso raw");
                }
            }
        });

        btnLeerFicheroSd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String linea = "";
                    EditText editText = (EditText) findViewById(R.id.nombreArchivo);
                    String nombreArchivo = editText.getText().toString();

                    File ruta_sd = Environment.getExternalStorageDirectory();

                    File f = new File(ruta_sd.getAbsolutePath(), nombreArchivo);

                    BufferedReader fin = new BufferedReader(
                            new InputStreamReader(
                                    new FileInputStream(f)));


                    while ((linea = fin.readLine()) != null) {
                        Log.i("Ficheros", "Texto: " + linea);
                    }
                    fin.close();
                    Log.i("Ficheros", "Fichero leido!");

                } catch (IOException ex) {
                    Log.e("Ficheros", "Error al leer el fichero de memoria interna");
                }

            }
        });

        btnEscribirFichero.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                {
                    try {


                        File f = new File(nombre);

                        OutputStreamWriter fout = new OutputStreamWriter(openFileOutput(f.getName(), Context.MODE_PRIVATE));

                        fout.write(contenido);

                        fout.close();

                        Log.i("Ficheros", "Fichero creado!");

                    } catch (Exception ex) {

                        Log.e("Ficheros", "Error al escribir fichero a memoria interna");
                    }
                }
            }
        });

        btnEscribirFicheroSD.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                {

                    try {
                        File ruta_sd = Environment.getExternalStorageDirectory();
                        File f = new File(ruta_sd.getAbsolutePath(), nombre);
                        OutputStreamWriter fout = null;
                        fout = new OutputStreamWriter(new FileOutputStream(f));
                        fout.write(contenido);
                        fout.close();
                    } catch (Exception ex) {
                        Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
                    }
                }
            }

        });
    }
}
