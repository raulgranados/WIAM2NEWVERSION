package org.escoladeltreball.iam43533099.myapplication;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamoros on 2/1/16.
 */
public class RssParserPull {


    private URL rssUrl; // Emmagatzemem la URL

	/*
     * Constructor de la classe rep com a argument l'enllaç url
	 */

    public RssParserPull(String url) {
        try {
            this.rssUrl = new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    /*
	 * Mètode que extreu les diferents notícies que conté una certa url (feed, rss)
	 * retorna una col·lecció (List) de news a partir de la URL que se li passa
	 */

    public List<New> parse() {
        List<New> news = null;
        XmlPullParser parser = Xml.newPullParser();

        try {

            parser.setInput(this.getInputStream(), null);

            int event = 0;

            event = parser.getEventType();

            New currentNew = null;

            // Mentre no arribem al tag final

            while (event != XmlPullParser.END_DOCUMENT) {
                String etiqueta = null;
                switch (event) {

                    case XmlPullParser.START_DOCUMENT:

                        news = new ArrayList<New>();
                        break;

                    case XmlPullParser.START_TAG:

                        etiqueta = parser.getName();

                        if (etiqueta.equals("item")) {
                            currentNew = new New();
                        } else if (currentNew != null) {
                            try {

                                if (etiqueta.equals("link")) {
                                    currentNew.setLink(parser.nextText());
                                } else if (etiqueta.equals("description")) {
                                    currentNew.setDescription(parser.nextText());
                                } else if (etiqueta.equals("pubDate")) {
                                    currentNew.setDate(parser.nextText());
                                } else if (etiqueta.equals("title")) {
                                    currentNew.setTitle(parser.nextText());
                                } else if (etiqueta.equals("guid")) {
                                    currentNew.setGuid(parser.nextText());
                                }
                            } catch (XmlPullParserException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        break;

                    case XmlPullParser.END_TAG:

                        etiqueta = parser.getName();

                        if (etiqueta.equals("item") && currentNew != null) {
                            news.add(currentNew);

                        }
                        break;
                }
                event = parser.next();
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return news;
    }

    private InputStream getInputStream() {
        try {

            return rssUrl.openConnection().getInputStream();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
