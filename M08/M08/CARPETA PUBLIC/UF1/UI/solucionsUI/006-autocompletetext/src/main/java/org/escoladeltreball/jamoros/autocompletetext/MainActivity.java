package org.escoladeltreball.jamoros.autocompletetext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

/*
        String[] filosofs = { "Karl Marx", "Platón", "Sigmund Freud",
				"Friedrich Nietzsche", "Friedrich Engels", "Benedetto Croce",
				"Jean Paul Sartre", "Ernst Cassirer",
				"Georg Wilhelm Friedrich Hegel", "Jürgen Habermas",
				"Thomas Hobbes", "Michel Foucault", "Mahatma Gandhi",
				"Martin Heidegger", "Confucio", "Noam Chomsky",
				"Jean-Jacques Rousseau", "Henry Adams", "Ludwig Wittgenstein",
				"Rudolf Carnap", "Ramon Llull", "Friedrich Schiller",
				"Aristóteles", "Immanuel Kant", "Mencio", "Jacques Lacan",
				"John Stuart Mill", "Karl Popper", "Thomas Kuhn", "Socrates",
				"John Locke", "Anthony Giddens", "Francis Bacon",
				"Arthur Schopenhauer", "René Descartes", "Thomas Paine",
				"Benjamin Franklin", "Erich Fromm", "George Santayana",
				"Howard Gardner", "David Hume", "José Ortega y Gasset",
				"William James", "Roger Bacon", "Peter Singer", "Ayn Rand",
				"Adam Smith", "Mikhail Bakhtin", "Carl Jung", "Jeremy Bentham",
				"Giordano Bruno", "Edmund Husserl", "Bertrand Russell",
				"Howard Zinn", "Carl Gustav Jung", "Wilhelm Wundt",
				"Jean Piaget", "Hans-Georg Gadamer", "Timothy Leary",
				"Erasmo de Rotterdam", "Gilles DeLeuze", "Rabindranath Tagore",
				"Margaret Mead", "Herbert Spencer", "Henri Poincaré",
				"Georg Simmel", "Voltaire", "Gottlob Frege",
				"Pierre de Coubertin", "Claude Bernard", "Jules Michelet",
				"John Dewey", "Averroes", "Edward Gibbon", "Manuel Castells",
				"John Rawls", "Max Scheler", "Johann Gottfried Herder",
				"Herbert Marcuse", "Samuel Taylor Coleridge", "Hannah Arendt",
				"Thomas Carlyle", "Paulo Freire", "Blaise Pascal",
				"Baruch Spinoza", "Fernand Braudel", "Paul Tillich",
				"Simone de Beauvoir", "Friedrich Schleiermacher",
				"Denis Diderot", "Gilberto Freyre", "Alfred North Whitehead",
				"Umberto Eco", "Robert Boyle", "Niall Ferguson",
				"Claude Lévi-Strauss", "Melanie Klein", "Ernst Bloch",
				"Jacques Derrida", "Francisco Suárez" };

*/

        String[] filosofs = this.getResources().getStringArray(R.array.filosofs_array);

        // Get a reference to the AutoCompleteTextView in the layout
        AutoCompleteTextView textView = (AutoCompleteTextView) findViewById(R.id.autocompleteFilosofs);

        // Create the adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, filosofs);

        //Specifies the minimum number of characters the user has to type in the edit box before the drop down list is shown.
        textView.setThreshold(3);

        //		Amb aquesta opció hi ha un problema amb el color de la font, es pot arreglar de moltes maneres, una d'elles es posant el theme Black
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//				android.R.layout.simple_list_item_1, filosofs);
        //and set it to the AutoCompleteTextView
        textView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
