package org.escoladeltreball.jamoros.ratingextra;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    LinearLayout rating;
    CheckBox star;
    int maxStar = 5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rating = (LinearLayout) findViewById(R.id.linLayout);

        for (int i = 1; i <= maxStar; i++) {
            star = (CheckBox) rating.findViewWithTag(String.valueOf(i));
            star.setOnClickListener(this);
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        CheckBox chkBox = (CheckBox) view;
        int tagInt = Integer.parseInt(chkBox.getTag().toString());

        Log.d("tagInt", "value of tagInt" + tagInt);

        for (int i = 1; i <= tagInt; i++) {
            star = (CheckBox) rating.findViewWithTag(String.valueOf(i));
            star.setChecked(true);
        }
        for (int i = tagInt + 1; i <= maxStar; i++) {
            Log.d("i", "1:value of i" + i);
            star = (CheckBox) rating.findViewWithTag(String.valueOf(i));
            star.setChecked(false);
        }
    }
}

/*
    We can add an element to the view just in case we want to reset te number of stars
 */
/*
    AppCompatActivity with API 16 get bad text orientation in the textview. Another dessign is done in activity_main2.xml
 */