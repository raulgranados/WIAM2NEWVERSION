package org.escoladeltreball.jamoros.llistaopcions2;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MyListActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_my_list);
        String[] values = new String[]{"Android", "iOS", "Windows Phone", "Blackberry", "Sailfish OS", "Tizen",
                "Firefox OS", "Ubuntu Touch", "Symbian", "Palm OS", "Maemo", "MeeGo"};

        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - the Array of data
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);

        // Assign adapter to ListView
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
        // l The ListView where the click happened
        // v The view that was clicked within the ListView
        // position The position of the view in the list
        // id The row id of the item that was clicked

        Toast.makeText(this, "Click ListItem Number " + position, Toast.LENGTH_SHORT).show();
    }

}
