package org.escoladeltreball.jamoros.temperatureconverterv1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android.support.design.widget.Snackbar;


public class TemperatureConverterActivity extends AppCompatActivity {

    EditText etCelsius;
    private EditText etFahrenheit;
    private Button btCelsius;
    private Button btFahrenheit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_converter);

        // Recuperem els widgets
        etCelsius = (EditText) findViewById(R.id.celsius_temp);
        etFahrenheit = (EditText) findViewById(R.id.fahrenheit_temp);

        btCelsius = (Button) findViewById(R.id.toCelsiusButton);
        btFahrenheit = (Button) findViewById(R.id.toFahrenheitButton);

        btCelsius.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                try {

                    double fahrenheit = Double.parseDouble(etFahrenheit
                            .getText().toString());
                    double celsius = (fahrenheit - 32.0) * 5.0 / 9.0;
                    etCelsius.setText(String.valueOf(celsius));
                } catch (Exception e) {
                    Toast.makeText(TemperatureConverterActivity.this,
                            R.string.failFahrenheit, Toast.LENGTH_SHORT).show();
                    // We have a problem with the value/no value in the celsius edittext, so we
                    // request the focus there for helping the user
                    etFahrenheit.requestFocus();
                }
            }
        });

        btFahrenheit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    double celsius = Double.parseDouble(etCelsius.getText()
                            .toString());
                    double fahrenheit = celsius * 9.0 / 5.0 + 32;
                    etFahrenheit.setText(String.valueOf(fahrenheit));
                } catch (Exception e) {
/*                    Toast.makeText(TemperatureConverterActivity.this,
                            "Celsius invalid", Toast.LENGTH_SHORT).show();
                    // We have a problem with the value/no value in the celsius edittext, so we
                    // request the focus there for helping the user
                    etCelsius.requestFocus();
*/
                    // We use Snackbar here just for learning

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myRelativeLayout),
                            "Celsius invalid", Snackbar.LENGTH_LONG);

//                    mySnackbar.setAction(R.string.retryCelsius, new MyCorrectListener());
                    mySnackbar.setAction(R.string.retryCelsius, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            etCelsius.requestFocus();
                        }
                    });
                    mySnackbar.show();
                }
            }
        });
    }

/*
    class MyCorrectListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            etCelsius.requestFocus();
        }
    }
*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_temperature_converter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

/* Note:
 A http://developer.android.com/training/snackbar/action.html we can read the next text:
 A Snackbar automatically goes away after a short time, so you can't count on the user seeing the message or having a chance to press the button. For this reason, you should consider offering an alternate way to perform any Snackbar action.
 In our example we make an action that helps the user to insert numbers inside the EditText,
 but he had to guess what the problem is with the message.
 */