package org.escoladeltreball.jamoros.radiobuttonextra;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnReset = (Button) findViewById(R.id.buttonReset);
        btnReset.setOnClickListener(this);

        Button btnResult = (Button) findViewById(R.id.buttonResult);
        btnResult.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Button btn = (Button) view;
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGrup);

        if (btn.getId() == R.id.buttonReset) {
            radioGroup.clearCheck();
        } else {
            int choice = radioGroup.getCheckedRadioButtonId();
            if (choice != -1) {
                Toast.makeText(
                        this,
                        ((RadioButton) findViewById(radioGroup
                                .getCheckedRadioButtonId())).getText(),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "no choice",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
