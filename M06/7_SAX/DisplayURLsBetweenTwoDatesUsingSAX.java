package query20;

/****************************************************************************
 * Copyright 2015 Ramón Bruballa <rbruballa@escoladeltreball.org> <rbruballa@gmail.com>                
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * 
 * @author rbruballa
 * 
 * This program displays the URLs modified between two given dates passed as arguments to the program
 * Dates must be expressed as YYYY-MM-DD, separated by "-"
 * 
 * Checking on valid input parameters (dates) was not a requirement
 * 
 * 		it works as well when start date is incomplete e.g. 2010-08
 * 		if end date is incomplete it will be treated as "before" instead of "before of equal"
 * 
 *  Usage:
 *  
 *  	DisplayURLsBetweenTwoDatesUsingSAX fromDate toDate
 *  
 *  		example:
 *  
 *  	DisplayURLsBetweenTwoDatesUsingSAX 2011-15-02 2014-23-09  
 *
 */


public class DisplayURLsBetweenTwoDatesUsingSAX {
	public static void main(String[] args){

		if (args.length!=2){
			System.err.println("Usage:");
			System.err.println("\n\tDisplayURLsBetweenTwoDatesUsingSAX fromDate toDate");
			System.err.println("\n\t\texample:");
			System.err.println("\n\tDisplayURLsBetweenTwoDatesUsingSAX 2011-15-02 2014-23-09");
			System.err.println("\n\t\tstart date can be incomplete e.g. 2010-08");
			System.err.println("\n\t\ta incomplete end date will be treated as  \"before\" instead of \"before of equal\"");
			System.exit(1);
		}
		
		// checking on start date <= end date and valid dates would be performed here

		try {	
			File inputFile = new File("data/sitemap-datasets-opendata.go.ke0.xml");
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			UserHandler userhandler = new UserHandler(args[0],args[1]);
			saxParser.parse(inputFile, userhandler);     
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(2);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			System.exit(3);
		} catch (SAXException e) {
			e.printStackTrace();
			System.exit(4);
		}
		
	}   
}