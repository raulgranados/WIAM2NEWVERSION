package query20;
/****************************************************************************
 * Copyright 2015 Ramón Bruballa <rbruballa@escoladeltreball.org> <rbruballa@gmail.com>                
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 ****************************************************************************/

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class UserHandler extends DefaultHandler {

	boolean bLoc = false;
	boolean bLastmod = false;
	String sLastmod,sLoc=null;
	String fI,fF; // dates d'inici i final

	
	public UserHandler(String fI, String fF) {
		this.fI= fI;
		this.fF = fF;
	}
	
	@Override
	public void startElement(String uri, 
			String localName, String qName, Attributes attributes)
					throws SAXException {

		if (qName.equalsIgnoreCase("loc")) {
			bLoc = true;
	 		sLastmod=null;
		} else if (qName.equalsIgnoreCase("lastmod")) {
			bLastmod = true;
		}
		
	}

	@Override
	public void endElement(String uri, 
			String localName, String qName) throws SAXException {

		if (qName.equalsIgnoreCase("url")) {

			if( sLastmod!=null &&
					sLastmod.compareTo(fI)>=0 &&
					sLastmod.compareTo(fF)<=0  )

				System.out.println( sLoc );

			sLoc=null;
		}

	}


	@Override
	public void characters(char ch[], 
			int start, 
			int length) throws SAXException {

		if (bLastmod) {

			sLastmod = new String(ch, start, length);
			bLastmod = false;
			
		} else if (bLoc) {

			sLoc = new String(ch, start, length);
			bLoc = false;
			
		} 

	}
}