package com.rbruballa.hibernate;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;  
import org.hibernate.SessionFactory;  
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;

/**
 * 
 * 
 * @author El teu nom
 * 
 * tot ha de funcionar inclús si la entrada en majúscules o minúscules
 * o les dades són majúscules o minúscules 
 * 
 * 
 * TODO
 * Que em funciona:
 * 
 * 
 * 
 *
 */

public class Exam {

	private static Session session;
	private final static int MAX_RESULTS=3;
	
	public Exam(Session session){
		Exam.session=session;
		System.out.println(cityNamesStartingBy("kr"));
		System.out.println(cityNamesFromCountryStartingBy("F","a"));
		System.out.println(updateCityNamed("amiens","amiens2"));
		deleteCitiesNamed("milano");
		
	}
	
	/**
	 * Returns a String containing the city names alphabetically ordered 
	 * that start by a given prefix
	 * 
	 * all results are included, upper or lower case input or table content
	 * does not matter 
	 *  
	 * no spaces, comma separated
	 *  
	 * a maximum of MAX_RESULTS is returned
	 * if there are more, ",.." is added and the rest ignored
	 * 
	 * no checks on valid input data needed
	 * 
	 * Example if MAX_RESULTS is 3
	 * 
	 * "Kraków,Kramators´k,Krasnodar,.."
	 * 
	 * 
	 * @param prefix
	 * @return String empty if none
	 */
	public static String cityNamesStartingBy(String prefix){
 
		
		return "";
		
	}
	

	/**
	 * 
	 * same as cityNamesStartingBy restricted to a given country code 
	 * 											(not country name)
	 * 
	 * 
	 * example "F","a"
	 * 
	 * 	"Aix-en-Provence,Ajaccio,Amiens,.."
	 * 
	 * @param country
	 * @param prefix
	 * @return
	 * 
	 */
	public static String cityNamesFromCountryStartingBy(String countryCode,String prefix){
		

		return "";
		
	}
	
	/**
	 * 
	 * Updates the name of given city only if there is exactly one city with
	 * the given name
	 * 
	 * no checks on valid input data needed
	 * 
	 * 
	 * @param oldName
	 * @param newName
	 * @return		 0	if success
	 * 				-1  if no such name
	 * 				-2	if there are more than one cities named oldName
	 * 				-3	other errors
	 */
	public static int updateCityNamed(String oldName,String newName){
		

		return -3;
		
	}
	
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static void deleteCitiesNamed(String name){
		
 
		
	}
		

	
}
