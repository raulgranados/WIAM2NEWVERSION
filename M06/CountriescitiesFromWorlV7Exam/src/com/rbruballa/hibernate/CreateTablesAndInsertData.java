package com.rbruballa.hibernate;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.xml.xpath.XPathExpressionException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.w3c.dom.Document;

public class CreateTablesAndInsertData {


	private static final String PATH_TO_XML_FILE = "data/world.xml";
	
	public static boolean CreatedataBaseAndInsertData() throws XPathExpressionException, SQLException, IOException {
	
		Document doc;

		Configuration cfg=new Configuration();  
		cfg.configure("hibernateCreate.cfg.xml"); 

		// aquí es donde peta si falla conexión con Postgres
		//creating session factory object  
		SessionFactory factory=cfg.buildSessionFactory();  

		//creating session object  
		Session session=factory.openSession();  

		//creating transaction object  
		Transaction t=session.beginTransaction();  

		doc=World.getDocument(PATH_TO_XML_FILE);

		// carreguem el document amb DOM perquè la majoria ho heu fet així
		// es una mostra de com es pot fer eficient amb aquesta lliberia
		// sinò, fer-lo amb SAX hagués estat una bona opció
		World.insertCountriesAndCitiesFromXML(doc,session);

		t.commit();

		session.close();  

		factory.close();
		
		File file =new File(Main.DDBB_CREATED);
		
		return file.createNewFile();
		
	}
	
}
