package com.rbruballa.hibernate;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.xml.xpath.XPathExpressionException;

import org.hibernate.HibernateException;
import org.hibernate.Session;  
import org.hibernate.SessionFactory;  
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;

public class Main {

	public static final String DDBB_CREATED="DBAlreadyCreated"; 

	public static void main(String[] args) throws XPathExpressionException, SQLException, HibernateException, IOException

	{

		if (!new File(DDBB_CREATED).exists()) {
			CreateTablesAndInsertData.CreatedataBaseAndInsertData();
		}

		Configuration cfg=new Configuration();  
		cfg.configure("hibernateUpdate.cfg.xml"); 

		//creating session factory object  
		SessionFactory factory=cfg.buildSessionFactory();  

		//creating session object  
		Session session=factory.openSession();  

		//creating transaction object  
		Transaction t=session.beginTransaction();  

		new Exam(session);

		t.commit();

		session.close();  

		factory.close(); 

		System.out.println("END");
	}


}
