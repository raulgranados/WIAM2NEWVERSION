package RaulGranadosExamenM06UF1;
/**
 * Copyright 2015  RaulGranados <raulgranados752@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3. See
 * http://www.gnu.org/licenses/gpl.html for more information.
 */


import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.Scanner;

public class Liga {

	/**

	  A la competició es vol emmagatzemar per a cada partit:

		  El resultat: cada equip pot marcar com a molt 200 punts (2bytes)x2
		  El nom de l'àrbitre: màx 30 caràcters
		  La data del partit: YYMMDD (6 bytes)

	  El menú del programa permet:

		  Veure els resultats del partits jugats fins al moment
		  Introduir el resultat d'un partit pendent de jugar
		  Seleccionar l'idioma de presentació de dades entre 4 disponibles
		  Re-iniciar la lliga, donant-li el nom del patrocinador i la temporada


	 */

	private static Scanner sc;
	private static RandomAccessFile RAFPartits;

	// contendra la temporada, el patrocinador y el idioma en que se mostraran los datos 
	private static File metadadesLliga;

	// nuevo objeto Properties, que sirve para obtener el valor de las variebles de los archivos properties.
	private static Properties propTranslate;
	
	// las siguientes variables se utilizaran para crear un objeto liga;
	private static String idiomaActual="CA", //idiomaActual contiene el idioma, por defecto sera el "CA";
						  temporada="",
						  patrocinador=""; 

	final static String DIRECTORY_DADES="";

	// l'idioma, temporada, patrocinador es guarden en arxiu amb writeUTF
	
	//final static String NOM_ARXIU_METADADES="lliga.dat";
	final static String NOM_ARXIU_METADADES="lliga.dat";
	// (alternativa : guardar en format editable com lliga.properties)
	
	//numero de equipos;
	final static int NUM_EQUIPS=11;

	//tamaño que ocupa un partido en el RAF
	final static int RECORD_SIZE=74;

	// per centrar una mica la capçalera
	final static String relleno= "\t"+ajustaString("",NUM_EQUIPS*10);

	
	//-----------------------MAIN--------------------------------------//
	
	public static void main(String[] args) throws IOException {
		System.out.println("Raul Granados Viladot");
		sc = new Scanner(System.in);
		
		// se crea un File para guardar con writeUTF el idioma, la temporada y el patrocinador
		// este File ha de ser DataInputStream (ya que captura de teclado) , ver [*1]
		metadadesLliga = new File(DIRECTORY_DADES+NOM_ARXIU_METADADES);
		
		
		RAFPartits = new RandomAccessFile(DIRECTORY_DADES+"Partits", "rw");
		propTranslate = new Properties();

		if (metadadesLliga.exists()) {
			
			// carrega l'idioma, la temporada i patrocinador
			try (DataInputStream in = new DataInputStream(new FileInputStream(
					metadadesLliga))) {

				idiomaActual  = in.readUTF();
				temporada=in.readUTF();
				patrocinador=in.readUTF();

				translateLoad(idiomaActual);
				mostrarElsPartits();

			} catch (Exception e) {
				System.err.println("probable corruption in metadata file");
				e.printStackTrace();
			}

		} else {

			translateLoad(idiomaActual);
			novaLliga();

		}
 
		
		// 				menu
		int op = 0;

		while (op != 5) {

			System.out.println("\n"+propTranslate.getProperty("menu"));

			while (!sc.hasNextInt()) {
				sc.nextLine();
				// TODO translate message
				System.err.println("\nSomething went wrong with the input");

			}

			op = sc.nextInt();


			switch (op) {

			case 1:
				
				mostrarElsPartits();
				break;

			case 2: // enter match info and save to file if correct

				try {
					System.out.println(propTranslate.getProperty("equips"));
					short local = sc.nextShort();
					short visitant = sc.nextShort();

					if (local < 1 || local > NUM_EQUIPS ||
							visitant < 1 || visitant > NUM_EQUIPS ||
							local==visitant ){

						System.err.println(propTranslate.getProperty("nointro"));

					} else {

						System.out.println(propTranslate.getProperty("resultats"));
						short puntslocal = sc.nextShort();
						short puntsvisitant = sc.nextShort();

						if (puntslocal < 0 || puntslocal > 200 ||
								puntsvisitant < 0	|| puntsvisitant > 200){

							System.err.println(propTranslate.getProperty("nointro"));

						}	else {

							System.out.println(propTranslate.getProperty("data"));
							String data = sc.next();

							SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
							df.setLenient(false);
							try {
								df.parse(data);	
								System.out.println(propTranslate.getProperty("arbitre"));
								String arbitre = sc.nextLine().trim();
								while (arbitre.length()==0){
									arbitre = sc.nextLine().trim();
								}

								writeMatchToFile(local, visitant, puntslocal,
										puntsvisitant, data, arbitre);

								mostrarElsPartits();	

							} catch (ParseException e) {
								System.err.println(propTranslate.getProperty("nointro"));
							}

						}
					}
				} catch ( java.util.InputMismatchException e) {

					// TODO translate message
					System.err.println("\nSomething went wrong with the input");

					System.err.println(propTranslate.getProperty("nointro"));
				}

				break;

			case 3:// switch language

				System.out.println("CA - ES - EN - FR");
				idiomaActual = sc.next();

				// TODO comprovar que l'idioma entrat és un de la llista d'idiomes disponibles

				guardaMetadadesLliga();
				translateLoad(idiomaActual);
				break;
				
			case 5://Llistar tota la informació dels partits jugats d'un equip concret entrat per teclat
				long offset=0;
				System.out.print("Introduzca el numero del equipo [ 0 - "+NUM_EQUIPS+"]: ");
				int equipo=sc.nextInt();
				if (equipo < 0 ||equipo > NUM_EQUIPS  ){
					System.out.println("Introduzca un numero de equipo correcto");
					equipo=sc.nextInt();
				} else {
				for (int i = 0; i < NUM_EQUIPS; i++) {
					
					offset=((i) * RECORD_SIZE + (equipo-1) * RECORD_SIZE*NUM_EQUIPS);
					//System.out.println("offset: "+offset);
					RAFPartits.seek(offset);
					//System.out.println(">"+RAFPartits.getFilePointer());
					if (RAFPartits.readShort() != -1){
						RAFPartits.seek(offset);
						//System.out.println(">"+RAFPartits.getFilePointer());
						System.out.println("1er Resultado: "+RAFPartits.readShort());
						RAFPartits.seek(offset+2);
						System.out.println("2do Resultado: "+RAFPartits.readShort());
						RAFPartits.seek(offset+4);
						System.out.println("Fecha: "+RAFPartits.readUTF());
						System.out.println("Nombre del arbitro: "+RAFPartits.readUTF());
						//System.out.print(RAFPartits.readUTF());
					}
				}	
					for (int j = 0; j < NUM_EQUIPS; j++) {
						
							offset=((equipo-1) * RECORD_SIZE + (j) * RECORD_SIZE*NUM_EQUIPS);
							//System.out.println("offset: "+offset);
							RAFPartits.seek(offset);
							//System.out.println(">"+RAFPartits.getFilePointer());
							if (RAFPartits.readShort() != -1){
								RAFPartits.seek(offset);
								//System.out.println(">"+RAFPartits.getFilePointer());
								System.out.println("1er Resultado: "+RAFPartits.readShort());
								RAFPartits.seek(offset+2);
								System.out.println("2do Resultado: "+RAFPartits.readShort());
								RAFPartits.seek(offset+4);
								System.out.println("Fecha: "+RAFPartits.readUTF());
								System.out.println("Nombre del arbitro: "+RAFPartits.readUTF());
								//System.out.print(RAFPartits.readUTF());
							}	
					}
				}
				break;
			case 6: //saber cuantos puntos acumula un equipo
				offset=0;
				int puntos=0;
				System.out.print("Introduzca el numero del equipo [ 0 - "+NUM_EQUIPS+"]: ");
				equipo=sc.nextInt();
				if (equipo < 0 ||equipo > NUM_EQUIPS  ){
					System.out.println("Introduzca un numero de equipo correcto");
					equipo=sc.nextInt();
					
				}else{
					
					for (int i = 0; i < NUM_EQUIPS; i++) {
						offset=((i) * RECORD_SIZE + (equipo-1) * RECORD_SIZE*NUM_EQUIPS);
							RAFPartits.seek(offset);
							if(RAFPartits.readShort()!=-1){
								RAFPartits.seek(offset);
								int res1=RAFPartits.readShort();
								//System.out.println(res1);
								RAFPartits.seek(offset+2);
								int res2=RAFPartits.readShort();
								//System.out.println(res2);
								if (res1<res2){
									puntos= puntos+3;
								} else	
								if (res1==res2){
									puntos+=1;
								}
							}
							
							for (int j = 0; j < NUM_EQUIPS; j++) {
								offset=((equipo-1) * RECORD_SIZE + (j) * RECORD_SIZE*NUM_EQUIPS);
									RAFPartits.seek(offset);
									if(RAFPartits.readShort()!=-1){
										RAFPartits.seek(offset);
										int res1=RAFPartits.readShort();
										//System.out.println(res1);
										RAFPartits.seek(offset+2);
										int res2=RAFPartits.readShort();
										//System.out.println(res2);
										if (res1>res2){
											puntos= puntos+3;
										} else	
										if (res1==res2){
											puntos+=1;
										}
									}	
							}		
					}
				System.out.println("El total de puntos del equipo "+equipo+" es : "+puntos);
				}
				break;
			case 4: 

				novaLliga();
				break;

			default:
				break;
			}
		}

		try {
			RAFPartits.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	// mostrar per pantalla els partits
	private static void mostrarElsPartits() {
		try {

			//  print capçalera lliga
			if (temporada.length()>0){ // poster encara no ha estat creada
				System.out.println(new StringBuilder().append("\n").append(relleno).append("\t").
						append(temporada).append(" ").
						append(propTranslate.getProperty("patrocinatPer")).append(" ").append(patrocinador).
						append("\n").append(relleno).append("* * * * * * * * * * * * * * * * * *\n"));
			}

			// equips locals
			StringBuilder sB=new StringBuilder();
			sB.append("\t\t");
			for (int i = 0; i < NUM_EQUIPS; i++) {
				sB.append("\t").append(propTranslate.getProperty("equip")).append(i + 1).append("    \t");
			}

			sB.append("\t(").append(propTranslate.getProperty("local")).append(")");
			System.out.println(sB);

			long offset=0;
			for (int i = 0; i < NUM_EQUIPS; i++) {

				for (int j = 0; j < NUM_EQUIPS; j++) {

					RAFPartits.seek(offset);
					
					//si j==0 -> imprime en la columna j=0 [cabecera de la tabla] equipo1 .. equipoN
					if (j == 0) {
						System.out.println();
						System.out.print(propTranslate.getProperty("equip") + (i+1)  + " \t");
					}
					 
					//si i=j -> no imprime nada porque corresponde a un partido donde local = visitante
					if(i==j){
						System.out.print("\t\t\t");
					
					//en cualquier otro caso imprime la info del partido	
					}else{
						
						

						short puntsPrimerEquip=RAFPartits.readShort();
						if (puntsPrimerEquip>-1){

							sB=new StringBuilder();
							sB.append(puntsPrimerEquip).append("-").append(RAFPartits.readShort());

							String date = RAFPartits.readUTF();

							sB.append(" ").append(date.substring(0, 2)).append("/").
							append(date.substring(2, 4)).append("/").append(date.substring(4, 6)).append(" ").
							append(RAFPartits.readUTF());//con este ultimo readUTF, obtiene el nombre del arbitro

							System.out.print(j!=NUM_EQUIPS-1?ajustaString(sB.toString(),24):sB);

						}else

							System.out.print(propTranslate.getProperty("nojugat"));
					}
					offset += RECORD_SIZE;
				}
			}

			System.out.println();


		}  catch ( EOFException | StringIndexOutOfBoundsException | java.io.UTFDataFormatException e) {
			System.err.println("\ncorrupted league file\n"+propTranslate.getProperty("reinici"));
		} catch (IOException e) {
			e.printStackTrace();
		}  catch ( Exception e) {
			System.err.println("\ncorrupted league file\n"+propTranslate.getProperty("reinici"));
			e.printStackTrace();
		}
		

	}


	/**
	 * ajusta un string al tamaño deseado para mejorar el aspecto de la impresión en formato tabular 
	 * 
	 * 	se añaden espacios por la derecha
	 * 
	 * 		WARNING	es posible q se recorte y se pierdan datos! 
	 * 
	 * @param s
	 *            String que queremos ajustar
	 *            
	 * @param n
	 *            int tamaño requerido
	 *            
	 * @return String modificado y del tamaño esperado
	 * 
	 */
	public static String ajustaString(String s, int n) {

		if (s.length() < n) {

			StringBuilder sBuilder = new StringBuilder();
			sBuilder = sBuilder.append(s);
			for (int i = s.length(); i < n; i++) {
				sBuilder = sBuilder.append(" ");
			}
			return sBuilder.toString();

		} else if (s.length() > n) {
			return s.substring(0, n);
		} else {
			return s;
		}
	}

	// Con el idioma que nos pasan por parametro, construimos la ruta del archivo properties correspondiente;
	// final String nomFitxerIdioma=DIRECTORY_DADES+"lang_"+idioma + ".properties";
	public static void translateLoad(String idioma) {
		FileInputStream in;

		final String nomFitxerIdioma=DIRECTORY_DADES+"lang_"+idioma + ".properties";

		//Carga el fichero lang_idioma.properties  a traves del objeto propTranslate de tipo Properties a 
		//traves del metodo .load(in)
		//una vez cargado, ya podriamos recuperar el valor de las variables del archivo con 
		//System.out.println(prop.getProperty("nombreDeVariable"));
		if (new File(nomFitxerIdioma).exists()) {
			try {
				in = new FileInputStream(nomFitxerIdioma);
				propTranslate.load(in);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
	}

	// guardar metadades lliga, inclou idioma
	public static void guardaMetadadesLliga() {

		try (RandomAccessFile out = new RandomAccessFile(metadadesLliga, "rw")) {
			out.seek(0);
			out.writeUTF(idiomaActual);
			out.writeUTF(temporada);
			out.writeUTF(patrocinador);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 *	Reset de la lliga
	 *
	 * 		Esborra els partits, demana info de patrocinador i temporada
	 *  
	 */
	public static void novaLliga() {

		System.out.println(propTranslate.getProperty("patrocinador"));
		patrocinador = sc.next();

		System.out.println(propTranslate.getProperty("temporada"));
		temporada = sc.next();

		guardaMetadadesLliga();

		try {

			int pos=0;

			for (int i = 0; i < NUM_EQUIPS*NUM_EQUIPS; i++) {

				RAFPartits.seek(pos);

				// marca de pemdent de jugar
				RAFPartits.writeShort(-1);

				pos+=RECORD_SIZE;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeMatchToFile(short local, short visitant, short puntsLocal,
			short puntsVisitant, String data, String arbitre) {

		// TODO demanar confirmació de sobreescritura en cas de que la informació d'aquest partit s'hagués entrat abans

		try {

			RAFPartits.seek((local - 1) * RECORD_SIZE + (visitant - 1) * RECORD_SIZE*NUM_EQUIPS);

			RAFPartits.writeShort(puntsLocal);
			RAFPartits.writeShort(puntsVisitant);
			RAFPartits.writeUTF(data);

			// arbitre màx 30 caràcters
			if (arbitre.length() > 30)
				arbitre = arbitre.substring(0, 30);

			RAFPartits.writeUTF(arbitre);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}



