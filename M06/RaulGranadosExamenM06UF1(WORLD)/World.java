package RaulGranadosExamenM06UF1;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*
A partir del document world.xml, mostra el països que tenen
 un GDP (PIB) més gran o igual a una quantitat donada
frontera amb altre donat (country code)
informació de població posterior o igual a un any donat
informació de població posterior o igual a un any donat i la població está en un rang donat
 */

public class World {
	public static void main(String[] args) {
		System.out.println("RAUL GRANADOS VILADOT");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder;
		Document doc = null;

		// doc.normalizeDocument();

		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse("world.xml");

			// Create XPathFactory object
			XPathFactory xpathFactory = XPathFactory.newInstance();

			// Create XPath object
			XPath xpath = xpathFactory.newXPath();

			
			
			List<String> borders = getCountriesHavingBorderWithSea(doc, xpath,"P");
			System.out.println("the  sought element are:" + Arrays.toString(borders.toArray()));



		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

	} 
		
	private static List<String> getCountriesHavingBorderWithSea(Document doc,
            XPath xpath, String pais) {
		
		List<String> list = new ArrayList<>(); 
        try {
            //create XPathExpression object
            XPathExpression expr =
                   xpath.compile("world/country[name="+pais+"]/city/name[@watertype='sea']");
            //evaluate expression result on XML document
            NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++)
                list.add(nodes.item(i).getNodeValue());
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        
        return list;
    }

	

	


}

