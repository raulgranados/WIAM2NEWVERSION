package com.rbruballa.hibernate;

import org.hibernate.Session;  
import org.hibernate.SessionFactory;  
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;  

public class Main {

	public static void main(String[] args) {
		System.out.println("..");

		Configuration cfg=new Configuration();  
		cfg.configure("hibernate.cfg.xml"); 

		// aquí es donde peta si falla conexión con Postgres
		//creating seession factory object  
		SessionFactory factory=cfg.buildSessionFactory();  

		//creating session object  
		Session session=factory.openSession();  

		//creating transaction object  
		Transaction t=session.beginTransaction();  

		Empleat e1=new Empleat();  
		e1.setId(120);  
		e1.setFirstName("Joan");  
		e1.setLastName("Samsó");
		e1.setPhone("60123456789");

		session.persist(e1);

		Empleat e2=new Empleat(); 
		e2.setId(121);  
		e2.setFirstName("Waine");  
		e2.setLastName("Woniack");  

		session.persist(e2);

		Empleat e3=new Empleat(); 
		e3.setId(122);  
		e3.setFirstName("Joel");  
		e3.setLastName("Garcia"); 
		e3.setPhone("93123456789"); 

		session.persist(e3);

		session.delete(e2);

		Empleat e4=new Empleat(); 
		e4.setId(123);  
		e4.setFirstName("L");  
		e4.setLastName("Lang");  

		session.persist(e4);
		
		t.commit();

		session.close();  

		factory.close();

 //		new TestEmpleat();

		System.out.println("END");
	}

}
