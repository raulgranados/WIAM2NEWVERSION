package com.rbruballa.hibernate;


import java.util.List; 
import java.util.Date;
import java.util.Iterator; 
 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class TestEmpleat {
   private static SessionFactory factory;
   
   public TestEmpleat() {

	      try {
	          factory = new Configuration().configure().buildSessionFactory();
	       }catch (Throwable ex) { 
	          System.err.println("Failed to create sessionFactory object." + ex);
	          throw new ExceptionInInitializerError(ex); 
	       }

	       /* afegim uns quants empleats */
	       Integer empID1 = addEmployee(200,"Felip", "Faus", "6234567");
	       Integer empID2 = addEmployee(201,"Dino", "Swen", "6345678");
	       Integer empID3 = addEmployee(202,"Liz", "Lz", "924353448");

	       /* Update employee's records */
	       modificaEmpleat(empID1, "930123456");

	       /* Delete an employee from the database */
	       esborraEmpleat(empID2);
	       
	       factory.close();
	   
   }
   
   
   /* Method to CREATE an employee in the database */
   public Integer addEmployee(int id,String fname, String lname, String phone){
      Session session = factory.openSession();
      Transaction tx = null;
      Integer employeeID = null;
      try{
         tx = session.beginTransaction();
         Empleat employee = new Empleat(id,fname, lname, phone);
         employeeID = (Integer) session.save(employee); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
      return employeeID;
   }
   

   
   /* canviar el telefon d'un empleat */
   public void modificaEmpleat(Integer EmployeeID, String phone ){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         Empleat employee = 
                    (Empleat)session.get(Empleat.class, EmployeeID); 
         employee.setPhone( phone );
		 session.update(employee); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
   }
   
   /* esborrar un empleat */
   public void esborraEmpleat(Integer EmployeeID){
      Session session = factory.openSession();
      Transaction tx = null;
      try{
         tx = session.beginTransaction();
         Empleat employee = 
                   (Empleat)session.get(Empleat.class, EmployeeID); 
         session.delete(employee); 
         tx.commit();
      }catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      }finally {
         session.close(); 
      }
   }
}
