db.books2.drop()

db.books2.insert([{
title: 'MongoDB Introduction',
description: 'MongoDB, a noSQL database',
by: ['Ramón Bruballa'],
mail: ['rbruballa@escoladeltreball.org','rbruballa@gmail.com'],
tags: ['mongodb', 'learning', 'database', 'noSQL'],
views: 100
},
{
title: 'Postgresql Up & Running',
description: 'Postgresql crash course',
by: ['Regina Obe','Leo Hsu'],
mail: [],
tags: ['postgresql', 'learning', 'database', 'SQL'],
isbn:'1449326293',
views: 200
},
{
title: 'Practical Postgresql',
description: 'Postgresql practical hands on',
by: ['Joshua D. Drake', '‎John C. Worsley '],
mail: [],
tags: ['postgresql', 'learning', 'database', 'SQL'],
views: 300
},
{
title: 'PostGIS in Action',
description: 'tips for geographical information systems',
by: ['Regina Obe','Leo Hsu'],
mail: [],
tags: ['GIS', 'learning'],
isbn:'1617291390',
views: 400
}])

