package com.rbruballa.hibernate;

import java.sql.SQLException;
import java.util.HashSet;

import javax.xml.xpath.XPathExpressionException;

import org.hibernate.Session;  
import org.hibernate.SessionFactory;  
import org.hibernate.Transaction;  
import org.hibernate.cfg.Configuration;  
import org.w3c.dom.Document;

public class Main {

	private static final String PATH_TO_XML_FILE = "data/world.xml";
	
	public static void main(String[] args) throws XPathExpressionException, SQLException {

		Document doc;

		Configuration cfg=new Configuration();  
		cfg.configure("hibernate.cfg.xml"); 

		// aquí es donde peta si falla conexión con Postgres
		//creating session factory object  
		SessionFactory factory=cfg.buildSessionFactory();  

		//creating session object  
		Session session=factory.openSession();  

		//creating transaction object  
		Transaction t=session.beginTransaction();  

/*		Country p3=new Country(); 
		p3.setCountryId("I");  
		p3.setCountryName("Italia");  

		City c1=new City(p3,"Roma");  
		City c2=new City(p3,"Milano");  
		City c3=new City(p3,"Firenze");  
		City c4=new City(p3,"Napoli");  

		HashSet<City> hs=new HashSet<City>();
		hs.add(c1);
		hs.add(c2);
		hs.add(c3);
		hs.add(c4);
		p3.setCities(hs);

		session.persist(p3);

		p3=new Country(); 
		p3.setCountryId("D");  
		p3.setCountryName("Deutschland");  

		c1=new City(p3,"Berlin");  
		c2=new City(p3,"Münich");  
		c3=new City(p3,"Hamburg");
		hs=new HashSet<City>();
		hs.add(c1);
		hs.add(c2);
		hs.add(c3);
		p3.setCities(hs);

		session.persist(p3);*/


		doc=World.getDocument(PATH_TO_XML_FILE);

		// carreguem el document amb DOM perquè la majoria ho heu fet així
		// es una mostra de com es pot fer eficient amb aquesta lliberia
		// sinò, fer-lo amb SAX hagués estat una bona opció
		World.insertCountriesAndCitiesFromXML(doc,session);
		
		t.commit();

		session.close();  

		factory.close(); 

		System.out.println("END");
	}

}
