/**

 *
 * Copyright 2015 .. <author@RaulGranados>
 *               
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.

*/
package RandomFiles;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Properties;
import java.util.Scanner;

public class Liga {
	String patrocinador;
	String temporada;
	int numeroDeEquipos;
	final int TAMAÑO_FICHERO;
	RandomAccessFile raf;

	/*
	 * Constructor
	 */

	public Liga(String patrocinador, String temporada, int numeroDeEquipos) {

		this.patrocinador = patrocinador;
		this.temporada = temporada;
		this.numeroDeEquipos = numeroDeEquipos;
		this.TAMAÑO_FICHERO= numeroDeEquipos*numeroDeEquipos*44;
		String nombreArchivo=this.patrocinador+this.temporada;
		try{
			raf = new RandomAccessFile(nombreArchivo,"rw");

			for (int i = 0; i < TAMAÑO_FICHERO; i=i+44) {
				String strA="______________________________";
				String strB="______";
				raf.seek(i);
				raf.writeShort(-1);
				raf.writeShort(-1);
				raf.writeUTF(strA);
				raf.writeUTF(strB);
			}
		}catch (EOFException e){
			System.out.println("END_FILE_ERROR "+e);
			e.printStackTrace();

		}catch (IOException exc){
			System.out.println("I/O Error "+exc);
			exc.printStackTrace();

		}
	}

	public RandomAccessFile getRaf() {
		return raf;
	}


	public int getTAMAÑO_FICHERO() {
		return TAMAÑO_FICHERO;
	}

	public void menu() {
		System.out.println("ACCESO A LOS PARTIDOS");
		System.out.println();
		System.out.println("1. VER RESULTADOS");
		System.out.println("2. INTRODUCIR UN RESULTADO");
		System.out.println("3. SELECCIONAR IDIOMA");
		System.out.println("4. REINICIAR");
		System.out.println("0. SALIR");
		System.out.println(" ESCOJA UNA OPCION (0 - 4): ");
	}

	public static void main(String[] args) throws IOException {
		Liga p = new Liga("patrocinador", "temporada2",10);
		p.menu();

		Scanner sc = new Scanner(System.in);
		sc.useDelimiter("\n");
		int op = sc.nextInt();

		do{
			switch (op) {
			case 1:
				try {

					for(int i=0; i<p.getTAMAÑO_FICHERO();i=i+44){
						p.getRaf().seek(i);
						System.out.println("\n\nEl resultado fue: ");
						System.out.print( p.getRaf().readShort()+" (LOCAL)");
						p.getRaf().seek(i+2);
						System.out.print(" / ");
						System.out.print(p.getRaf().readShort()+ " (VISITANTE)");
						System.out.println("\nEl arbitro fue: ");
						p.getRaf().seek(i+4);
						System.out.print((p.getRaf().readUTF()));
						System.out.println("\nSe jugo el dia: ");
						p.getRaf().seek(i+36);
						System.out.println((p.getRaf().readUTF()));
					}
				}catch (Exception e){
					System.out.println("Error de tipo: "+e);
					e.printStackTrace();
				}
				p.menu();

				sc = new Scanner(System.in);
				sc.useDelimiter("\n");

				op = sc.nextInt();
				break;


			case 2:

				sc = new Scanner(System.in);
				System.out.println("Introduzca el nombre del equipo local: ");
				int equipoLocal = sc.nextInt();
				boolean controlEquipoLocal = false;
				while(!controlEquipoLocal){
					if (0>=equipoLocal||equipoLocal>p.numeroDeEquipos){
						sc = new Scanner(System.in);
						System.out.println("ERROR");
						System.out.println("Debes introducir un numero entre 1 y "+p.numeroDeEquipos);
						System.out.println("Introduzca de nuevo el nombre del equipo local: ");
						equipoLocal = sc.nextInt();
					}else {
						controlEquipoLocal=true;
					}
				}
				sc = new Scanner(System.in);
				System.out.println("Introduzca el nombre del equipo visitante: ");
				int equipoVisitante = sc.nextInt();

				boolean controlEquipoVisitante = false;
				while(!controlEquipoVisitante){
					if (0>=equipoVisitante||equipoVisitante>p.numeroDeEquipos){
						sc = new Scanner(System.in);
						System.out.println("ERROR");
						System.out.println("Debes introducir un numero entre 1 y "+p.numeroDeEquipos);
						System.out.println("Introduzca de nuevo el nombre del equipo visitante: ");
						equipoVisitante = sc.nextInt();
					}else {
						controlEquipoVisitante=true;
					}
				}
				int posicion=((equipoLocal-1)*p.numeroDeEquipos+(equipoVisitante-1))*44;

				sc = new Scanner(System.in);
				System.out.println("Introduzca el nombre del arbitro[max 30 chars]: ");

				String arbitro = sc.next();
				if(arbitro.length()>30){ 
					arbitro=arbitro.substring(0,30);
				}
				else { arbitro=arbitro.substring(0,arbitro.length());}
				sc = new Scanner(System.in);
				System.out.println("Introduzca  el resultado del equipo local[max 200 pts]: ");

				Short resultado1 = sc.nextShort();
				boolean controlResultado1= false;
				while(!controlResultado1){
					if (resultado1<0||resultado1>999){
						sc = new Scanner(System.in);
						System.out.println("ERROR");
						System.out.println("Debes introducir un numero entre 0 y 999");
						System.out.print("Introduce de nuevo el resultado del equipo local: ");
						resultado1 = sc.nextShort();
					}else{ controlResultado1=true;}
				}
				sc = new Scanner(System.in);
				System.out.println("Introduzca  el resultado del equipo visitante[max 200 pts]: ");
				Short resultado2 = sc.nextShort();
				boolean controlResultado2= false;
				while(!controlResultado2){
					if (resultado2<0||resultado2>200){
						sc = new Scanner(System.in);
						System.out.println("ERROR");
						System.out.println("Debes introducir un numero entre 0 y 200");
						System.out.print("Introduce de nuevo el resultado del equipo visitante: ");
						resultado2 = sc.nextShort();
					}else{ controlResultado2=true;}
				}

				sc = new Scanner(System.in);
				System.out.println("Introduzca la fecha[DDMMYY]: ");
				String fecha = sc.next();
				if(fecha.length()>6){fecha=fecha.substring(0,6);}
				else { fecha=fecha.substring(0,fecha.length());}

				try{
					p.getRaf().seek(posicion);

					p.getRaf().writeShort(resultado1);
					p.getRaf().writeShort(resultado2);
					p.getRaf().writeUTF(arbitro);
					p.getRaf().seek(posicion+36);
					p.getRaf().writeUTF(fecha);
				}catch (IOException exc){

					System.out.println("I/O Error "+exc);
					try {
						p.getRaf().close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}


				System.out.println();

				p.menu();

				sc = new Scanner(System.in);
				sc.useDelimiter("\n");

				op = sc.nextInt();

				break;
			case 3:

				sc = new Scanner(System.in);

				System.out.println("ESPAÑOL pulse ES;");
				System.out.println("CATALAN pulse CA;");
				System.out.println("ITALIANO pulse IT;");
				System.out.println("INGLES pulse IN;");	

				System.out.println("Que idioma desea: ");

				String idioma = sc.next();

				Properties prop = new Properties();
				FileInputStream input = new FileInputStream(idioma+".properties");
				prop.load(input);
				System.out.println(prop.getProperty("saludo"));
				p.menu();
				sc = new Scanner(System.in);
				sc.useDelimiter("\n");

				sc = new Scanner(System.in);
				sc.useDelimiter("\n");

				op = sc.nextInt();
				break;	
			case 4:
				try{
					for (int i = 0; i < p.TAMAÑO_FICHERO; i=i+44) {
						String strA="______________________________";
						String strB="______";
						p.raf.seek(i);
						p.raf.writeShort(-1);
						p.raf.writeShort(-1);
						p.raf.writeUTF(strA);
						p.raf.writeUTF(strB);
					}
				}catch (EOFException e){
					System.out.println("END_FILE_ERROR "+e);
					e.printStackTrace();

				}catch (IOException exc){
					System.out.println("I/O Error "+exc);
					exc.printStackTrace();

				}

				p.menu();

				sc = new Scanner(System.in);
				sc.useDelimiter("\n");

				op = sc.nextInt();


				break;


			case 0:

				System.out.println("HA ELEGIDO SALIR DEL PROGRAMA");
				p.getRaf().close();
				break;

			default:

				System.out.println("OPCION INCORRECTA");

				break;
			}	
		} while (op !=0);

	}
}