package RandomFiles;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class DemoRAF {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		RandomAccessFile raf = new RandomAccessFile("demoRAF","rw");
		try{ 
			
			raf.seek(0);
			raf.writeByte(1);
			raf.seek(0);
			System.out.println(raf.readByte());
			System.out.println(raf.getFilePointer());
			System.out.println(raf.length());
		}catch (IOException exc){
			
			System.out.println("I/O Error "+exc);
			try {
				raf.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	}
}
