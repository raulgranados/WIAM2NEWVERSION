package Files;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class GestionDeFicheros {

	public static void main(String[] args) throws ParseException {

		File f1=new File("2015");

		File f2=new File("src");	
		
		System.out.println("Responde si f1 es un archivo: "+f1.isFile());
		System.out.println("Responde si f1 es un directorio: "+f1.isDirectory());
		System.out.println("Responde si f2 es un archivo: "+f2.isFile());
		System.out.println("Responde si f2 es un directorio: "+f2.isDirectory());
		
		// nombreFichero.list():
		
		// retorna un array con el contenido del directorio.
		// al tratarse de un array, para mostrar el resultado por pantalla, debemos llamar al 
		// metodo Arrays.toString.
		
		System.out.println("El resultado de listar la carpeta es: "+Arrays.toString(f2.list()));
		
		// nombreFichero.listFiles() te devuelve la ruta.
		
		//System.out.println(Arrays.toString(f2.listFiles()));
		
		//nombreFichero.length():
		
		//retorna el tamaño en bytes del fichero.
		
		System.out.println("La longuitd del fichero en bytes es: "+f1.length());
		
		// nombreFichero.lastModified():
		
		// retorna un long con la fecha en que se modifico por última vez
		// para pasar el long a Date:
		// Date d = new Date(long);
		
		System.out.println("La fecha en la que se modifico por última vez el archivo: "+new Date(f1.lastModified()));
		
		// Modificar la fecha de modificación de un archivo	
		
		String string_date = "12-12-2016";

		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		Date d = f.parse(string_date);
		long milliseconds = d.getTime();
		
		System.out.println("El String en milisegundos es: "+milliseconds);
		
		System.out.println(f1.setLastModified(milliseconds));
		
		// isHidden:
		
		// retorna si el fichero es oculto, o no.
		
		System.out.println("Se trata de un fichero oculto?: "+f1.isHidden());
		
		// gestionar permisos
		
		System.out.println(f1.canRead());
		System.out.println(f1.canWrite());
		System.out.println(f1.canExecute());
		
		System.out.println(f2.getFreeSpace());	
		
		File f3=new File("src","dirHijo");
		System.out.println(f3.mkdir());
		
	}
}
