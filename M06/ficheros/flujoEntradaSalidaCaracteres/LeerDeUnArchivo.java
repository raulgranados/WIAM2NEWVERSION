package flujoEntradaSalidaCaracteres;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeerDeUnArchivo {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		String s;
		
		try (BufferedReader br = new BufferedReader(new FileReader("Texto.txt")))
		{
			while ((s=br.readLine())!=null){
				System.out.println(s);
			}
				
		} catch (IOException exc){
			
			System.out.println("I/O Error "+exc);
			
		} 
	}

}
