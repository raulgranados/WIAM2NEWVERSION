package flujoEntradaSalidaCaracteres;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LeerCaracteres {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))){
			System.out.println("Introduzca caracters, para finalizar pulse .");
			do {
				char c= (char)br.read();
				System.out.print(c);
			} while (br.read()!=-1 || br.read()=='.');
		}
	}

}
