package flujoEntradaSalidaCaracteres;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LeerLineas {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		try
			(BufferedReader br = new BufferedReader(new InputStreamReader(System.in)))
		{
			String linea;
			System.out.println("Introduzca varias líneas de texto, introduzca fin para acabar:");
			do{
				linea = br.readLine();
				System.out.println(linea);
			} while (!linea.equals("fin"));
		}
		
		
		

	}

}
