package flujoEntradaSalidaCaracteres;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class EscribirEnUnArchivo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		String str;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		
		System.out.println("Enter text ('stop' to quit)");
		
		try (FileWriter fw = new FileWriter("Texto.txt"))
		{
			do {
				str = br.readLine();
				if (str.compareTo("stop")!=0){
				str= str + "\r\n";
				fw.write(str);
				}else
					break;
			} while (str.compareTo("stop")!=0);
		}catch (IOException exc) {
			System.out.println("Error I/O Exception "+exc);		
				
		}
		}

	}


