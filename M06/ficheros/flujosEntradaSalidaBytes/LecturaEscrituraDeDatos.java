package flujosEntradaSalidaBytes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class LecturaEscrituraDeDatos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i=10;
		double d=1023.56;
		boolean b=true;
		
		//Escribir valores. Try con recursos.
		
		try (DataOutputStream flujoSalida=new DataOutputStream(
			new FileOutputStream("/home/raul/Documentos/WIAM2/M06/test1.txt")))
		{
		System.out.println("escribiendo " +i);
		flujoSalida.writeInt(i);
		System.out.println("escribiendo " +d);
		flujoSalida.writeDouble(d);
		System.out.println("escribiendo " +b);
		flujoSalida.writeBoolean(b);
		} catch (IOException exc) {
			System.out.println("Write error");
			return;
		}
		
		try (DataInputStream flujoEntrada=new DataInputStream(new FileInputStream("/home/raul/Documentos/WIAM2/M06/test1.txt")))
		{
			i=flujoEntrada.readInt();
			System.out.println("leyendo " +i);
			d=flujoEntrada.readDouble();
			System.out.println("leyendo " +d);
			b=flujoEntrada.readBoolean();
			System.out.println("leyendo " +b);
		}catch (IOException exc) {
			System.out.println("Read error");
			return;
		}
		

}
}