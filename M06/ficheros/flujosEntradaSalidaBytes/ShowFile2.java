package flujosEntradaSalidaBytes;

import java.io.FileInputStream;
import java.io.IOException;

public class ShowFile2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i;

		// En el siguiente bloque se usa un try con recursos para
		// abrir un archivo y después cerrarlo automáticamente al salir.

		try (FileInputStream archivoEntrada = new FileInputStream("/home/raul/Documentos/WIAM2/M06/test.txt")) {
			do {
				i = archivoEntrada.read();
				if (i != -1)
					System.out.print((char) i);

			} while (i != -1);

		} catch (IOException exc) {
			System.out.println("Error reading file.");
		}
	}

}
