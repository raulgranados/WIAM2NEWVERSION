package flujosEntradaSalidaBytes;

import java.io.*;

//Leer una matriz de bytes desde teclado.

public class ReadBytes {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		byte data[] = new byte[10];

		System.out.println("Enter some character: ");
		System.in.read(data); // lee una matriz de bytes desde teclado
		System.out.print("You entered: ");
		for (int i = 0; i < data.length; i++) // recorre la matriz de bytes
			System.out.print((char) data[i]);// realiza un cast de cada elemento
												// de la matriz y lo imprime

	}
}
