package flujosEntradaSalidaBytes;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class AccesoAleatoreoDemo {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		double [] data = {19.4,10.1,123.54,33.0,87.9,74.25};
		double d=2;
		
		
		try (RandomAccessFile raf = new RandomAccessFile("DemoAccessRandom.dat","rw"))
		{
			for(int i=0;i<data.length;i++){
				raf.writeDouble(data[i]);
			}
			
			raf.seek(0);
			d=raf.readDouble();
			System.out.println("El primer valor es: "+d);
			
			raf.seek(8);
			d=raf.readDouble();
			System.out.println("El segundo valor es: "+d);
			
			raf.seek(8*3);
			d=raf.readDouble();
			System.out.println("El cuarto valor es: "+d);
			
			
			
		}catch (IOException exc) {
			System.out.println("I/O Error: "+exc);
		}
	}

}
