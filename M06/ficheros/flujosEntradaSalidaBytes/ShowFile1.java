package flujosEntradaSalidaBytes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ShowFile1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i;
		FileInputStream fin;

		try {
			fin = new FileInputStream("/home/raul/Documentos/WIAM2/M06/test.txt");
		} catch (FileNotFoundException exc) {
			System.out.println("File Not Found");
			return;
		}

		try {
			do {
				i = fin.read();
				if (i != -1)
					System.out.print((char) i);

			} while (i != -1);

		} catch (IOException exc) {
			System.out.println("Error reading file.");
		} finally { // usamos el bloque finally para cerrar el archivo

			try {
				fin.close();
			} catch (IOException exc) {
				System.out.println("Error closing file.");
			}
		}
	}
}
