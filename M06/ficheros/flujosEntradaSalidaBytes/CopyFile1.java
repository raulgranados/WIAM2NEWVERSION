package flujosEntradaSalidaBytes;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFile1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i;

		try (FileInputStream archivoEntrada = new FileInputStream(
				"/home/raul/Documentos/WIAM2/M06/demoCopiarUnArchivo");
				FileOutputStream archivoSalida = new FileOutputStream(
						"/home/raul/Documentos/WIAM2/M06/imagenDemoCopiarUnArchivo1");) {
			do {
				i = archivoEntrada.read();
				if (i != -1)
					archivoSalida.write(i);
			} while (i != -1);
		} catch (IOException exc) {
			System.out.println("I/O Error: " + exc);
		}
	}

}
