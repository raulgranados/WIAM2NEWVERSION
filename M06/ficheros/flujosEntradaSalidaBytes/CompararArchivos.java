package flujosEntradaSalidaBytes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CompararArchivos {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		int i = 0, j = 0;

		try (FileOutputStream archivo1_escritura = new FileOutputStream("primero.txt");
				FileOutputStream archivo2_escritura = new FileOutputStream("segundo.txt");

				FileInputStream archivo1_lectura = new FileInputStream("primero.txt");
				FileInputStream archivo2_lectura = new FileInputStream("segundo.txt")) {
			
			for (int x=0;x<10;x++)
			archivo1_escritura.write((int)x);
			
			for (int x=0;x<10;x++)
				archivo2_escritura.write((int)x);
				
			
			do {
				i = archivo1_lectura.read();
				System.out.println(i);
				j = archivo2_lectura.read();
				if (i != j)
					return;
			} while (i != -1 && j != -1);

			if (i != j) {
				System.out.println("Los Archivos son diferentes");
			} else {
				System.out.println("Los archivos son iguales");
			}
		} catch (IOException exc) {
			System.out.println("I/O Error: " + exc);
		}

	}

}
