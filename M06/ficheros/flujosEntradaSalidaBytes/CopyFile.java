package flujosEntradaSalidaBytes;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i;
		FileInputStream archivoEntrada = null;
		FileOutputStream archivoSalida = null;

		try {
			archivoEntrada = new FileInputStream("/home/raul/Documentos/WIAM2/M06/demoCopiarUnArchivo");
			archivoSalida = new FileOutputStream("/home/raul/Documentos/WIAM2/M06/imagenDemoCopiarUnArchivo");
			do {
				i = archivoEntrada.read();
				if (i != -1)
					archivoSalida.write(i);
			} while (i != -1);
		} catch (IOException exc) {
			System.out.println("I/O Error: " + exc);
		} finally {
			try {
				if (archivoEntrada != null)
					archivoEntrada.close();
			} catch (IOException exc) {
				System.out.println("Error closing input file");
			}
			try {
				if (archivoSalida != null)
					archivoSalida.close();
			} catch (IOException exc) {
				System.out.println("Error closing output file");
			}
		}
	}
}
