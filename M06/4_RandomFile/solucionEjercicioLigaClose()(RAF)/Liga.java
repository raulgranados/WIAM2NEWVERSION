/**
 * Copyright 2015  Ramón Bruballa <rbruballa@gmail.com>   menu translations by Meritxell Martí <m.marblu@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3. See
 * http://www.gnu.org/licenses/gpl.html for more information.
 */
package raf.lliga;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.Scanner;

public class Liga {

	/**

	  A la competició es vol emmagatzemar per a cada partit:

		  El resultat: cada equip pot marcar com a molt 200 punts (2bytes)x2
		  El nom de l'àrbitre: màx 30 caràcters
		  La data del partit: YYMMDD (6 bytes)

	  El menú del programa permet:

		  Veure els resultats del partits jugats fins al moment
		  Introduir el resultat d'un partit pendent de jugar
		  Seleccionar l'idioma de presentació de dades entre 4 disponibles
		  Re-iniciar la lliga, donant-li el nom del patrocinador i la temporada


	 */

	private static Scanner sc;
	private static RandomAccessFile RAFPartits;

	// conté a més de temporada i patrocinador l'idioma de presentació de dades 
	private static File metadadesLliga;

	private static Properties propTranslate;
	private static String idiomaActual="CA", // idioma per defecte si s'ha esborrat l'arxiu de metadades
			temporada="",patrocinador=""; 

	final static String DIRECTORY_DADES="dades/lliga/";

	// l'idioma, nomLliga, patrocinador es guarden en arxiu amb writeUTF
	final static String NOM_ARXIU_METADADES="lliga.dat";
	// alternativa : guardar en format editable com lliga.properties

	final static int NUM_EQUIPS=11;

	final static int RECORD_SIZE=74;

	// per centrar una mica la capçalera
	final static String relleno= "\t"+ajustaString("",NUM_EQUIPS*10);

	public static void main(String[] args) throws IOException {

		sc = new Scanner(System.in);
		metadadesLliga = new File(DIRECTORY_DADES+NOM_ARXIU_METADADES);
		RAFPartits = new RandomAccessFile(DIRECTORY_DADES+"Partits", "rw");
		propTranslate = new Properties();

		if (metadadesLliga.exists()) {
			// carrega l'idioma, la temporada i patrocinador
			try (DataInputStream in = new DataInputStream(new FileInputStream(
					metadadesLliga))) {

				idiomaActual  = in.readUTF();
				temporada=in.readUTF();
				patrocinador=in.readUTF();

				translateLoad(idiomaActual);
				mostrarElsPartits();

			} catch (Exception e) {
				System.err.println("probable corruption in metadata file");
				e.printStackTrace();
			}

		} else {

			translateLoad(idiomaActual);
			novaLliga();

		}
 
		
		// 				menu
		int op = 0;

		while (op != 5) {

			System.out.println("\n"+propTranslate.getProperty("menu"));

			while (!sc.hasNextInt()) {
				sc.nextLine();
				// TODO translate message
				System.err.println("\nSomething went wrong with the input");

			}

			op = sc.nextInt();


			switch (op) {

			case 1:
				
				mostrarElsPartits();
				break;

			case 2: // enter match info and save to file if correct

				try {
					System.out.println(propTranslate.getProperty("equips"));
					short local = sc.nextShort();
					short visitant = sc.nextShort();

					if (local < 1 || local > NUM_EQUIPS ||
							visitant < 1 || visitant > NUM_EQUIPS ||
							local==visitant ){

						System.err.println(propTranslate.getProperty("nointro"));

					} else {

						System.out.println(propTranslate.getProperty("resultats"));
						short puntslocal = sc.nextShort();
						short puntsvisitant = sc.nextShort();

						if (puntslocal < 0 || puntslocal > 200 ||
								puntsvisitant < 0	|| puntsvisitant > 200){

							System.err.println(propTranslate.getProperty("nointro"));

						}	else {

							System.out.println(propTranslate.getProperty("data"));
							String data = sc.next();

							SimpleDateFormat df = new SimpleDateFormat("yyMMdd");
							df.setLenient(false);
							try {
								df.parse(data);	
								System.out.println(propTranslate.getProperty("arbitre"));
								String arbitre = sc.nextLine().trim();
								while (arbitre.length()==0){
									arbitre = sc.nextLine().trim();
								}

								writeMatchToFile(local, visitant, puntslocal,
										puntsvisitant, data, arbitre);

								mostrarElsPartits();	

							} catch (ParseException e) {
								System.err.println(propTranslate.getProperty("nointro"));
							}

						}
					}
				} catch ( java.util.InputMismatchException e) {

					// TODO translate message
					System.err.println("\nSomething went wrong with the input");

					System.err.println(propTranslate.getProperty("nointro"));
				}

				break;

			case 3:// switch language

				System.out.println("CA - ES - EN - FR");
				idiomaActual = sc.next();

				// TODO comprovar que l'idioma entrat és un de la llista d'idiomes disponibles

				guardaMetadadesLliga();
				translateLoad(idiomaActual);
				break;

			case 4: 

				novaLliga();
				break;

			default:
				break;
			}
		}

		try {
			RAFPartits.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	// mostrar per pantalla els partits
	private static void mostrarElsPartits() {
		try {

			//  print capçalera lliga
			if (temporada.length()>0){ // poster encara no ha estat creada
				System.out.println(new StringBuilder().append("\n").append(relleno).append("\t").
						append(temporada).append(" ").
						append(propTranslate.getProperty("patrocinatPer")).append(" ").append(patrocinador).
						append("\n").append(relleno).append("* * * * * * * * * * * * * * * * * *\n"));
			}

			// equips locals
			StringBuilder sB=new StringBuilder();
			sB.append("\t\t");
			for (int i = 0; i < NUM_EQUIPS; i++) {
				sB.append("\t").append(propTranslate.getProperty("equip")).append(i + 1).append("    \t");
			}

			sB.append("\t(").append(propTranslate.getProperty("local")).append(")");
			System.out.println(sB);

			long offset=0;
			for (int i = 0; i < NUM_EQUIPS; i++) {

				for (int j = 0; j < NUM_EQUIPS; j++) {

					RAFPartits.seek(offset);

					if (j == 0) {
						System.out.println();
						System.out.print(propTranslate.getProperty("equip") + (i+1)  + " \t");
					}
					 
					if(i==j){
						System.out.print("\t\t\t");
					}else{

						short puntsPrimerEquip=RAFPartits.readShort();
						if (puntsPrimerEquip>-1){

							sB=new StringBuilder();
							sB.append(puntsPrimerEquip).append("-").append(RAFPartits.readShort());

							String date = RAFPartits.readUTF();

							sB.append(" ").append(date.substring(0, 2)).append("/").
							append(date.substring(2, 4)).append("/").append(date.substring(4, 6)).append(" ").
							append(RAFPartits.readUTF());

							System.out.print(j!=NUM_EQUIPS-1?ajustaString(sB.toString(),24):sB);

						}else

							System.out.print(propTranslate.getProperty("nojugat"));
					}
					offset += RECORD_SIZE;
				}
			}

			System.out.println();


		}  catch ( EOFException | StringIndexOutOfBoundsException | java.io.UTFDataFormatException e) {
			System.err.println("\ncorrupted league file\n"+propTranslate.getProperty("reinici"));
		} catch (IOException e) {
			e.printStackTrace();
		}  catch ( Exception e) {
			System.err.println("\ncorrupted league file\n"+propTranslate.getProperty("reinici"));
			e.printStackTrace();
		}
		

	}


	/**
	 * ajusta un string al tamaño deseado para mejorar el aspecto de la impresión en formato tabular 
	 * 
	 * 	se añaden espacios por la derecha
	 * 
	 * 		WARNING	es posible q se recorte y se pierdan datos! 
	 * 
	 * @param s
	 *            String que queremos ajustar
	 *            
	 * @param n
	 *            int tamaño requerido
	 *            
	 * @return String modificado y del tamaño esperado
	 * 
	 */
	public static String ajustaString(String s, int n) {

		if (s.length() < n) {

			StringBuilder sBuilder = new StringBuilder();
			sBuilder = sBuilder.append(s);
			for (int i = s.length(); i < n; i++) {
				sBuilder = sBuilder.append(" ");
			}
			return sBuilder.toString();

		} else if (s.length() > n) {
			return s.substring(0, n);
		} else {
			return s;
		}
	}

	// carreguem la traducció del literals segons l'idioma actual
	public static void translateLoad(String idioma) {
		FileInputStream in;

		final String nomFitxerIdioma=DIRECTORY_DADES+"lang_"+idioma + ".properties";

		if (new File(nomFitxerIdioma).exists()) {
			try {
				in = new FileInputStream(nomFitxerIdioma);
				propTranslate.load(in);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 
	}

	// guardar metadades lliga, inclou idioma
	public static void guardaMetadadesLliga() {

		try (RandomAccessFile out = new RandomAccessFile(metadadesLliga, "rw")) {
			out.seek(0);
			out.writeUTF(idiomaActual);
			out.writeUTF(temporada);
			out.writeUTF(patrocinador);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 *	Reset de la lliga
	 *
	 * 		Esborra els partits, demana info de patrocinador i temporada
	 *  
	 */
	public static void novaLliga() {

		System.out.println(propTranslate.getProperty("patrocinador"));
		patrocinador = sc.next();

		System.out.println(propTranslate.getProperty("temporada"));
		temporada = sc.next();

		guardaMetadadesLliga();

		try {

			int pos=0;

			for (int i = 0; i < NUM_EQUIPS*NUM_EQUIPS; i++) {

				RAFPartits.seek(pos);

				// marca de pemdent de jugar
				RAFPartits.writeShort(-1);

				pos+=RECORD_SIZE;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeMatchToFile(short local, short visitant, short puntsLocal,
			short puntsVisitant, String data, String arbitre) {

		// TODO demanar confirmació de sobreescritura en cas de que la informació d'aquest partit s'hagués entrat abans

		try {

			RAFPartits.seek((local - 1) * RECORD_SIZE + (visitant - 1) * RECORD_SIZE*NUM_EQUIPS);

			RAFPartits.writeShort(puntsLocal);
			RAFPartits.writeShort(puntsVisitant);
			RAFPartits.writeUTF(data);

			// arbitre màx 30 caràcters
			if (arbitre.length() > 30)
				arbitre = arbitre.substring(0, 30);

			RAFPartits.writeUTF(arbitre);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
