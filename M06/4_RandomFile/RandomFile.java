package M06;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;

public class RandomFile{

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		String str;
		
		try (RandomAccessFile raf = new RandomAccessFile("EjemploAccesoArchivoAleatorio2.dat","rw"))

		{
			//for (int i=0; i<100; i++){

				raf.writeBytes("000-200, 123456789012345678901234567890, yymmdd");
				raf.writeBytes("000-200, 123456789012345678901234567890, dfgd");
			//}	
		
		
		raf.seek(0);

		//raf.writeUTF("000-200, 123456789012345678901234567890, yymmdd");
		
		str=raf.readUTF();

		System.out.println("El primer valor es: "+str);
		
		
		raf.seek(8);

		
		str=raf.readUTF();

		System.out.println("El primer valor es: "+str);
		
		
		} catch (IOException exc) {

			System.out.println("I/O Error: "+exc);

		}
		
		
	}

}
