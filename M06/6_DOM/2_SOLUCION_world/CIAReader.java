package cia;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

 
/*
 * A partir del document world.xml, mostra el països que tenen

un GDP (PIB) més gran o igual a una quantitat donada
frontera amb altre donat (country code)
informació de població posterior o igual a un any donat
informació de població posterior o igual a un any donat i la població está en un rang donat

 */

/**Copyright 2015 Daniel Cruz <i>
 * <a href="mailto:dani.cruz.morales@gmail.com">dani.cruz.morales@gmail.com</a><br><br>
 * 
 * This is free software, licensed under the GNU General Public License v3.<br>
 * See <a href="http://www.gnu.org/licenses/gpl.html">http://www.gnu.org/licenses/gpl.html</a> for more information.
 *
 */
public class CIAReader extends DomReader {

	public CIAReader(String xml) throws ParserConfigurationException, SAXException, IOException {
		super(xml);
		
	}
	
	public List<String> byGDP(int minGDP){
		return super.extractList("//country[gdp_total>="+minGDP+"]/name/text()");
	}
	
	public List<String> borders(String code){
		
		return super.extractList("//country[@car_code='"+code+"']/border/@country");
	}
	
	public List<String> havePopInfoSince(int year){
		
		return super.extractList("//country/population[@year >="+year+"]/../name/text()");
	}
	
public List<String> havePopInfoInRange(int year, int min, int max){
		
		return super.extractList("//country/population[@year >="+year+"][text()>="+min+"][text()<="+max+"]/../name/text()");
	}

}
