package cia;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * 
 * Copyright 2015 Daniel Cruz <i>
 * <a href="mailto:dani.cruz.morales@gmail.com">dani.cruz.morales@gmail.com</a><br><br>
 * 
 * This is free software, licensed under the GNU General Public License v3.<br>
 * See <a href="http://www.gnu.org/licenses/gpl.html">http://www.gnu.org/licenses/gpl.html</a> for more information.
 *
 * @author Daniel Cruz 
 *
 */
public class MainCIA {

	
	/*
	 * A partir del document world.xml, mostra el països que tenen

    un GDP (PIB) més gran o igual a una quantitat donada
    frontera amb altre donat (country code)
    informació de població posterior o igual a un any donat
    informació de població posterior o igual a un any donat i la població está en un rang donat

	 */
	
	public static void main(String[] args) throws ParserConfigurationException, 
														SAXException, IOException {
		
		CIAReader r=new CIAReader("/home/users/inf/wiam2/iam47100231/workspaces/M06/practicas/src/dom/cia/world.xml");
		int gdp=40000;
		System.out.println("Countries with min GDP "+gdp);
		DomReader.print(r.byGDP(gdp));
		System.out.println();
		
		String cod="E";
		System.out.println("Countries wich have boder with "+cod);
		DomReader.print(r.borders(cod));
		System.out.println();
		
		int year=2008;
		System.out.println("Countries with population info since "+year);
		DomReader.print(r.havePopInfoSince(year));
		System.out.println();
		
		int min=5000000;
		int max=6000000;
		System.out.println("Countries with population info since "+year+" and population in "+year+" between "+min+" and "+max);
		DomReader.print(r.havePopInfoInRange(year, min, max));
	}

}
