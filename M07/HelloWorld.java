/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene; // es la escena. El panel principal de la stage (frame)
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane; // layout, se colocan de arriba - abajo
import javafx.stage.Stage;  // ventana de la app, es como el jframe

/**
 *
 * @author iam43533099
 */

// http://javafx-omg.blogspot.com.es/2013/10/javafx-primera-ventana.html

// La clase debe extender de Application
public class HelloWorld extends Application {
    
    // El método start() es el que se encarga de crear los componentes y mostrar la ventana
    // Este método inicia todo, siempre recibe un parámetro tipo stage
    @Override
    public void start(Stage primaryStage) {
        
        // Definimos un control de tipo Button
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        
        
        // En JavaFX contamos con diversos eventos, como son: ActionEvent, WindowEvent, InputEvent, KeyEvent, MouseEvent.
        // Además contamos con interfaces EventHandler, ChangeListener, etc.
        // ActionEvent que es el evento estándar para este control(Button). 
        // Los eventos podemos asociarlos a los controles de 2 formas: Clase anónimas, o por referencia.
        // Uso de clase anónima:
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                
                // Aqui va la acción:
                // mostrara "Hello World!" por consola
                System.out.println("Hello World!");
            }
        });
        
        // Este será el layout del panel principal (Scene)
        StackPane root = new StackPane();
        
        // Asignamos el StackPane root (layout) a la escena y le da dimensiones
        Scene scene = new Scene(root, 300, 250);
        
        // Agregamos el botón al layout (StackPane)
        root.getChildren().add(btn);
        
        primaryStage.setTitle("Hello World!");
        
        // Añadimos la Scene (escena) al objeto Stage
        primaryStage.setScene(scene);
        
        // Muestra la ventana, el objeto Stage primaryStage (que hemos pasado 
        // como parametro del metodo start())
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Lanza la aplicación
        launch(args);
    }
    
}
