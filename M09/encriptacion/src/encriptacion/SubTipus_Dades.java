package encriptacion;

public class SubTipus_Dades {

	int id;
	private String nom;
	private int valor;
	
	public SubTipus_Dades(int id, String nom, int valor) {
		this.id = id;
		this.nom = nom;
		this.valor = valor;
	}

	public SubTipus_Dades() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubTipus_Dades other = (SubTipus_Dades) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}