package encriptacion;

import java.util.LinkedList;
import java.util.List;

public class Conversa_Dades {
    int id;			//L'usuari no pot posar aquest nº, ho ha de fer el sistema i
				//si ha de posar un id nou mirarà en la BD quin és el més gran i li sumarà 1.
    String descripcio;		//La descripció o nom que tindrà la conversa perquè l'usuari la diferencii de les altres.
    boolean activa;		//Per defecte estarà a true.
    
    List<Missatge_Dades> llistaMissatges = new LinkedList<Missatge_Dades>();

    
    public Conversa_Dades(int id, String descripcio, boolean activa) {
        this.id = id;
        this.descripcio = descripcio;
        this.activa = activa;
    }

    
    public Conversa_Dades() {
		// TODO Auto-generated constructor stub
	}


	public int getId() {
        return id;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public boolean isActiva() {
        return activa;
    }

    public List<Missatge_Dades> getLlistaMissatges() {
        return llistaMissatges;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public void setLlistaMissatges(List<Missatge_Dades> llistaMissatges) {
        this.llistaMissatges = llistaMissatges;
    }
    
    public void sumaMissatge(Missatge_Dades m) {
        llistaMissatges.add(m);
    }
    
    public String toString(){
    	return new StringBuilder().append("Conversa: ID = "+this.getId()+" DESC = "+this.getDescripcio()+" (ACTIVA = "+isActiva()+" )").toString();
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conversa_Dades other = (Conversa_Dades) obj;
		if (id != other.id)
			return false;
		return true;
	}
}  
    
    
