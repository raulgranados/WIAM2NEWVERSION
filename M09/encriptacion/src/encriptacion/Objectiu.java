package encriptacion;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;




import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

public class Objectiu {
	

	// ID INICIAL PARA CADA CIUDAD
	
//	//Obtenir l'ultim id d'objectiu
	int getIDObjectius(int idCiudad){
		int id = 0;
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try {
			Ciutat_Dades cs = new Ciutat_Dades();
			cs.setId(idCiudad);
            ObjectSet<Ciutat_Dades> result = db.queryByExample(cs);
            if (result.hasNext()) {
            	Ciutat_Dades aux = result.next();
            	ArrayList<Objectiu_Dades> r = aux.getListObjectiuDades();
            	if (r.size()>0) {
	            	int index = r.size()-1;
	            	Objectiu_Dades od = r.get(index);
	                id = od.getId();
            	} else {
            		return 0;
            	}
            }
            
		} finally {
			db.close();
		}
		
		return id;
	}
	
	
	//NUNCA EXISTIRA UN REPETIDO ENTRE CIUDADES.
	
//	int getIDObjectius(){
//		int id = 0;
//		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
//		try {
//			Predicate<Objectiu_Dades> p = new Predicate<Objectiu_Dades>() { 
//                /**
//				 * 
//				 */
//				private static final long serialVersionUID = 1L;
//
//				@Override 
//                public boolean match(Objectiu_Dades c) { 
//                    return true; 
//                } 
//            }; 
//            
//            List<Objectiu_Dades> result = db.query(p); 
//            Objectiu_Dades od = result.get(result.size()-1);
//            id = od.getId()+1;
//                        
//		} finally {
//			db.close();
//		}
//		return id;
//	}
	
	
	//Menu31: Afegir un objectiu
	void menu31() {

			Scanner sc = new Scanner(System.in);
		
			Ciutat2.menu21();
			
			System.out.println("Selecciona la ciudad [ ID ]: ");
			
			int idCiudad = sc.nextInt();
			
			int id = getIDObjectius(idCiudad)+1;
			System.out.println("ID: "+id);
				
				System.out.println("ANULAT [FALSE OR TRUE]");
				boolean anulat = sc.nextBoolean();
				System.out.println("Tipus: ");
				int tipus = sc.nextInt();
				System.out.println("SubTipusID: ");
				int subTipusId = sc.nextInt();
				System.out.println("CoordX: ");
				int coordX = sc.nextInt();
				System.out.println("CoordY: ");
				int coordY = sc.nextInt();
				System.out.println("Descripcio: ");
				String descripcio =sc.next();
				
				Objectiu_Dades objectiuDades = new Objectiu_Dades(id, anulat, tipus, subTipusId, coordX, coordY, descripcio);
				
				System.out.println("@|| "+objectiuDades);
			
			Ciutat_Dades cs = new Ciutat_Dades();
			cs.setId(idCiudad);
			
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");	
		try {
		
			ObjectSet<Ciutat_Dades> c = db.queryByExample(cs);
				if (c.hasNext()) {
					Ciutat_Dades tmp = c.next();
					ArrayList<Objectiu_Dades> lod = tmp.getListObjectiuDades();
					lod.add(objectiuDades);
//					System.out.println("@|| "+tmp);
//					System.out.println("@|| "+lod.size());
					db.store(tmp.getListObjectiuDades());
				}
		} finally {
			db.close();
			//Ciutat2.menu22();
			System.out.println("MENU 31 FINAL");
		}
	}
	
	void menu32() {
		Scanner sc = new Scanner(System.in);
		Ciutat2.menu22();
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");	
		try {
			System.out.println("Introduce el id ciudad: ");
			int idCiudad = sc.nextInt();
			
			Ciutat_Dades cd = new Ciutat_Dades();
			cd.setId(idCiudad);
			
			ObjectSet<Ciutat_Dades> cResult = db.queryByExample(cd);
			if ( cResult.hasNext() ) {
				
				Ciutat_Dades cAux = cResult.next();
				
				System.out.println("Introduce el id objetivo: ");
				int idObjetivo = sc.nextInt();
				
				ArrayList<Objectiu_Dades> listObjectiu = cAux.getListObjectiuDades();
				
				for (int i = 0; i < listObjectiu.size(); i++) {
					Objectiu_Dades od =listObjectiu.get(i);
					if (od.getId() == idObjetivo) {
						//
						System.out.println("ANULAT [FALSE OR TRUE]");
						boolean anulat = sc.nextBoolean();
						System.out.println("Tipus: ");
						int tipus = sc.nextInt();
						System.out.println("SubTipusID: ");
						int subTipusId = sc.nextInt();
						System.out.println("CoordX: ");
						int coordX = sc.nextInt();
						System.out.println("CoordY: ");
						int coordY = sc.nextInt();
						System.out.println("Descripcio: ");
						String descripcio =sc.next();
						
						od.setAnulat(anulat);
						od.setTipus(tipus);
						od.setSubTipusId(subTipusId);
						od.setCoordX(coordX);
						od.setCoordY(coordY);
						od.setDescripcio(descripcio);
						
						db.store(cAux.getListObjectiuDades());
					
						break;
					}
				}
					
			} else {
				System.out.println("Ciudad incorrecta.");
			}
			
		} finally {
			db.close();
		}
	}
	
	void menu33 () {
		Scanner sc = new Scanner(System.in);
		Ciutat2.menu22();
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"
				+ File.separator + "BD-exercici.db4o");
		try {
			System.out.println("Introduce el id ciudad: ");
			int idCiudad = sc.nextInt();

			Ciutat_Dades cd = new Ciutat_Dades();
			cd.setId(idCiudad);

			ObjectSet<Ciutat_Dades> cResult = db.queryByExample(cd);
			if (cResult.hasNext()) {

				Ciutat_Dades cAux = cResult.next();

				System.out.println("Introduce el id objetivo: ");
				int idObjetivo = sc.nextInt();

				ArrayList<Objectiu_Dades> listObjectiu = cAux
						.getListObjectiuDades();

				for (int i = 0; i < listObjectiu.size(); i++) {
					Objectiu_Dades od = listObjectiu.get(i);
					if (od.getId() == idObjetivo) {
						//
						System.out.println("ANULAT [FALSE OR TRUE]");
						boolean anulat = sc.nextBoolean();
						od.setAnulat(anulat);
						db.store(cAux.getListObjectiuDades());
					}
				}
			} else {
				System.out.println("Ciudad incorrecta.");
			}

		} finally {
			db.close();
		}
	}


	public void menu34() {
		
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
    	try {
    	Predicate<Ciutat_Dades> p = new Predicate<Ciutat_Dades>() { 

			private static final long serialVersionUID = 1L;

			@Override 
            public boolean match(Ciutat_Dades o) { 
                return true; 
            } 
        }; 
        
        List<Ciutat_Dades> result = db.query(p); 
        
        if (result.size()>0) {
        
	        for(Ciutat_Dades cd : result){
	        	System.out.println(cd);
	        	List<Objectiu_Dades> objectius = cd.getListObjectiuDades();
	        	Iterator<Objectiu_Dades> it = objectius.iterator();
	        	while(it.hasNext()){
	        		System.out.println(it.next());
	        	}
	        }
        
        } else  {
        	System.out.println("No hi a cap Objectiu.");
        }
	        
    	} finally {
    		db.close();
    		System.out.println("FINAL MENU34()");
    	}
	}

}