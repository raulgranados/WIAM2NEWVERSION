package encriptacion;

public class Objectiu_Dades {
	
	private int id;
	private boolean anulat;
	private int tipus;
	private int subTipusId;
	private int coordX;
	private int coordY;
	private String descripcio;
	public Objectiu_Dades(int id, boolean anulat, int tipus, int subTipusId,int coordX, int coordY, String descripcio) {
		this.id = id;
		this.anulat = anulat;
		this.tipus = tipus;
		this.subTipusId = subTipusId;
		this.coordX = coordX;
		this.coordY = coordY;
		this.descripcio = descripcio;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isAnulat() {
		return anulat;
	}
	public void setAnulat(boolean anulat) {
		this.anulat = anulat;
	}
	public int getTipus() {
		return tipus;
	}
	public void setTipus(int tipus) {
		this.tipus = tipus;
	}
	public int getSubTipusId() {
		return subTipusId;
	}
	public void setSubTipusId(int subTipusId) {
		this.subTipusId = subTipusId;
	}
	public int getCoordX() {
		return coordX;
	}
	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}
	public int getCoordY() {
		return coordY;
	}
	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	@Override
	public String toString() {
		return "Objectiu_Dades [id=" + id + ", anulat=" + anulat + ", tipus="
				+ tipus + ", subTipusId=" + subTipusId + ", coordX=" + coordX
				+ ", coordY=" + coordY + ", descripcio=" + descripcio + "]";
	}
	
}