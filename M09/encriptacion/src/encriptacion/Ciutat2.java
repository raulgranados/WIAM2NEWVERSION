package encriptacion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

public class Ciutat2 {
	
	void menu11(ArrayList<Ciutat_Dades>ciutatLLista){
		ArrayList<Objectiu_Dades> obDades = new ArrayList<Objectiu_Dades>();
		File fitxer = new File("arxiusPerCarregar"+File.separator+"ciutatsVsObjectius.txt"); // Adreçament
		// relatiu
		FileReader fr = null; // Entrada
		BufferedReader br; // Buffer
		String liniaLLegida;
		String comandes[];
		String comandaLLegida = "";
		boolean llegidaClau;
		String ciutatId = "0";
		String ciutatNom = "";
		String ciutatCoordX = "0";
		String ciutatCoordY = "0";
		String objectiuId = "0";
		String objectiuTipus = "0";
		String objectiuSubTipusId = "0";
		String objectiuCoordX = "0";
		String objectiuCoordY = "0";
		String objectiuDescripcio = "";
		
		
		String countALL = "0";
		
		try {
			
			fr = new FileReader(fitxer); // Inicialitza entrada del fitxer
			br = new BufferedReader(fr); // Inicialitza buffer amb l’entrada
			while ((liniaLLegida = br.readLine()) != null) {
				comandes = liniaLLegida.split("=");
				llegidaClau = false;
				for (String comanda : comandes) {
					if (llegidaClau == false) {
						comandaLLegida = comanda;
						llegidaClau = true;
					} else {
						llegidaClau = false;
						switch (comandaLLegida) {
						case "ciutatId":
							if (countALL.equals("1")) {
								//System.out.println(Integer.parseInt(ciutatId)+" "+ciutatNom+" "+Integer.parseInt(ciutatCoordX)+" "+Integer.parseInt(ciutatCoordY)+" "+obDades);
								Ciutat_Dades cd = new Ciutat_Dades(Integer.parseInt(ciutatId), ciutatNom, Integer.parseInt(ciutatCoordX), Integer.parseInt(ciutatCoordY),obDades); 
								ciutatLLista.add(cd);
								System.out.println("Guardando ciudad #1 en lista ciudades");
								countALL="2";
								obDades.clear();								
							} 
							ciutatId = comanda;
							break;
						case "ciutatNom":
							ciutatNom = comanda;
							break;

						case "ciutatCoordX":
							ciutatCoordX = comanda;
							break;

						case "ciutatCoordY":
							ciutatCoordY = comanda;
							
							if (countALL.equals("0")) {
								countALL = "1";
							}			
							
							break;

						case "objectiuId":
							objectiuId = comanda;
							break;

						case "objectiuTipus":
							objectiuTipus = comanda;
							break;
						case "objectiuSubTipusId":
							objectiuSubTipusId = comanda;
							break;
						case "objectiuCoordX":
							objectiuCoordX = comanda;
							break;
						case "objectiuCoordY":
							objectiuCoordY = comanda;
							break;
						case "objectiuDescripcio":
							objectiuDescripcio = comanda;
							Objectiu_Dades od = new Objectiu_Dades(Integer.parseInt(objectiuId),true,Integer.parseInt(objectiuTipus), Integer.parseInt(objectiuSubTipusId)
									,Integer.parseInt(objectiuCoordX), Integer.parseInt(objectiuCoordY),objectiuDescripcio);
							obDades.add(od);
							break;
							
						}
					}
				}
			}
			
			if (ciutatId.equals(countALL)&&countALL.equals("2")) {
				//System.out.println(Integer.parseInt(ciutatId)+" "+ciutatNom+" "+Integer.parseInt(ciutatCoordX)+" "+Integer.parseInt(ciutatCoordY)+" "+obDades);
				Ciutat_Dades cd = new Ciutat_Dades(Integer.parseInt(ciutatId), ciutatNom, Integer.parseInt(ciutatCoordX), Integer.parseInt(ciutatCoordY),obDades);
				ciutatLLista.add(cd);
				System.out.println("Guardando ciudad #2 en lista ciudades");
				obDades.clear();				
			}	
			
		//System.out.println(aux);
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void menu12(ArrayList<Ciutat_Dades> ciutatLLista) {
		
		System.out.println("Tamaño ciutat: "+ciutatLLista.size());
		
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try {
			for (Ciutat_Dades ciutat : ciutatLLista) {
				ObjectSet<Ciutat_Dades> os = db.queryByExample(ciutat);
				if (!os.hasNext()) {
					db.store(ciutat);
					System.out.println("Insertando en la base de datos: "+ciutat);
				}
			}
		}finally{
			db.close();
		}
	}
	
	static void menu21() {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try{
			Predicate<Ciutat_Dades> p = new Predicate<Ciutat_Dades>() { 
                /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override 
                public boolean match(Ciutat_Dades c) { 
                    return true; 
                } 
            }; 
            
            List<Ciutat_Dades> result = db.query(p); 
            
            for (Ciutat_Dades c : result) { 
               System.out.println(
            		   new StringBuilder().append("[ ID: "+c.getId()+" NOM: "+c.getNom()+" COORDX: "+c.getCoordX()+" COORDY: "+c.getCoordY()+" ]").toString()
               );
            }
            
            db.close();
            
		}finally{
			db.close();
		}
	}
	
	static void menu22() {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try{
			Predicate<Ciutat_Dades> p = new Predicate<Ciutat_Dades>() { 
                /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override 
                public boolean match(Ciutat_Dades c) { 
                    return true; 
                } 
            }; 
            
            List<Ciutat_Dades> result = db.query(p); 
            
            for (Ciutat_Dades c : result) { 
               System.out.println(
            		   new StringBuilder().append(c).toString()
               );
            } 
		}finally{db.close();}
	}
	
	void menu23() {
		menu21();
		Scanner sc = new Scanner(System.in);
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try {
		System.out.println("Selecciona una ciudad por su id: ");
		int id = sc.nextInt();
		Ciutat_Dades cs = new Ciutat_Dades();
		cs.setId(id);
		ObjectSet<Ciutat_Dades> c = db.queryByExample(cs);
			if (c.hasNext()) {
				System.out.println(c);
			}
		}finally{
			db.close();
		}
	}
	
	int getLastIdFromCiudad(){
		int id = 0;
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try {
			Predicate<Ciutat_Dades> p = new Predicate<Ciutat_Dades>() { 
                /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override 
                public boolean match(Ciutat_Dades c) { 
                    return true; 
                } 
            }; 
            
            List<Ciutat_Dades> result = db.query(p); 
            Ciutat_Dades cd = result.get(result.size()-1);
            id = cd.getId()+1;
            return id;
            
		} finally {
			db.close();
		}
	}
	
	void menu24() {
		ArrayList<Objectiu_Dades> od = new ArrayList<Objectiu_Dades>();
		Scanner sc = new Scanner(System.in);
		int idCiudad = getLastIdFromCiudad();
		System.out.println("Nom: ");
		String nom = sc.next();
		System.out.println("CoordX: ");
		int coordX = sc.nextInt();
		System.out.println("CoordY: ");
		int coordY = sc.nextInt();
		Ciutat_Dades cd = new Ciutat_Dades(idCiudad, nom, coordX, coordY, od);
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try{
		db.store(cd);
		} finally {
			db.close();
		}
	}
	
	void menu25() {}
	void menu26() {
		Scanner sc = new Scanner(System.in);
		ArrayList<Objectiu_Dades> listObOri = new ArrayList<Objectiu_Dades>();
		ArrayList<Objectiu_Dades> listObDes = new ArrayList<Objectiu_Dades>();
		System.out.println("Ciutat origen [ID] : ");
		int idOrigen = sc.nextInt();
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try {
		Ciutat_Dades co = new Ciutat_Dades();
		co.setId(idOrigen);
		ObjectSet<Ciutat_Dades> origen = db.queryByExample(co);
		
		if (origen.hasNext()) {
			Ciutat_Dades aux = origen.next();
			listObOri = aux.getListObjectiuDades();
		}
		System.out.println("Ciutat destí [ID] : ");
		int idDestino = sc.nextInt();
		co.setId(idDestino);
		ObjectSet<Ciutat_Dades> destino = db.queryByExample(co);
		
		if (destino.hasNext()) {
			Ciutat_Dades aux = destino.next();
			listObDes = aux.getListObjectiuDades();
			listObDes.addAll(listObOri);
			db.store(aux.getListObjectiuDades());
		}

		} finally {
			db.close();
			System.out.println("MENU 26 FINAL");
		}
		
		
	}
	
//	public class customComparator implements Comparator<SubTipus_Dades> {
//	    public int compare(SubTipus_Dades object1, SubTipus_Dades object2) {
//	    	Integer x = object1.getValor();
//	        return x.compareTo(object2.getValor());
//	    }
//	}
	
	void menu27(ArrayList<Ciutat_Dades>ciutatLLista) {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try {
			
			Predicate<Ciutat_Dades> p = new Predicate<Ciutat_Dades>() { 
                /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override 
                public boolean match(Ciutat_Dades c) { 
                    return true; 
                } 
            }; 
            
            List<Ciutat_Dades> result = db.query(p); 
            
            for (Ciutat_Dades c : result) { 
            		ciutatLLista.add(c);
            } 
			
		TreeMap<Integer,Ciutat_Dades> sorted = new TreeMap<Integer,Ciutat_Dades>();
		int valor = 0;
				
		for (int i = 0; i < ciutatLLista.size(); i++) {
			Ciutat_Dades cd = ciutatLLista.get(i);
			System.out.println("CIUDAD: "+cd);
			ArrayList<Objectiu_Dades> od = ciutatLLista.get(i).getListObjectiuDades();
			for(int j = 0; j < od.size(); j++){
				int idSubTipus = od.get(j).getSubTipusId();
				SubTipus_Dades st = new SubTipus_Dades();
				st.setId(idSubTipus);
				ObjectSet<SubTipus_Dades> sd = db.queryByExample(st);
				if (sd.hasNext()) {
					SubTipus_Dades sdV = sd.next();
					valor += sdV.getValor();
				}
			}
			System.out.println("VALOR: "+valor);
			sorted.put(valor,cd);
			valor = 0;
		}
		
		Iterator <Entry<Integer,Ciutat_Dades>> it = sorted.entrySet().iterator();
		Entry<Integer,Ciutat_Dades> entry = null;
		while (it.hasNext()) {
			entry = it.next();
			System.out.println("@|"+entry.getKey());
		}
		} finally {
			db.close();
		}
		
	}
	
	void menu28() {
		Scanner sc = new Scanner(System.in);
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
		try {
			System.out.println("Ciutat ha eliminar [ID] : ");
			int idDel = sc.nextInt();
			Ciutat_Dades co = new Ciutat_Dades();
			co.setId(idDel);
			ObjectSet<Ciutat_Dades> del = db.queryByExample(co);
			db.delete(del.next());
		} finally {
			db.close();
			System.out.println("MENU 28 FINAL");
		}
	}
	
}
