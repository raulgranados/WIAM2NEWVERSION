package encriptacion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

public class Ciutat {

	//11. 
    void menu11(ArrayList<Ciutat_Dades> ciutatLLista) throws IOException {
		File fitxer = new File("arxiusPerCarregar"+File.separator+"ciutatsVsObjectius.txt"); // Adreçament
		// relatiu
		FileReader fr = null; // Entrada
		BufferedReader br; // Buffer
		String liniaLLegida;
		String comandes[];
		String comandaLLegida = "";
		boolean llegidaClau;
		String ciutatId = "0";
		String ciutatNom = "";
		String ciutatCoordX = "0";
		String ciutatCoordY = "0";
		String objectiuId = "0";
		String objectiuTipus = "0";
		String objectiuSubTipusId = "0";
		String objectiuCoordX = "0";
		String objectiuCoordY = "0";
		String objectiuDescripcio = "";
		ArrayList<Objectiu_Dades> objectius = new ArrayList<Objectiu_Dades>();
		
		TreeMap<Integer,ArrayList<Objectiu_Dades>> id = new TreeMap<Integer,ArrayList<Objectiu_Dades>>();

		try {
			fr = new FileReader(fitxer); // Inicialitza entrada del fitxer
			br = new BufferedReader(fr); // Inicialitza buffer amb l’entrada
			while ((liniaLLegida = br.readLine()) != null) {
				comandes = liniaLLegida.split("=");
				llegidaClau = false;
				for (String comanda : comandes) {
					if (llegidaClau == false) {
						comandaLLegida = comanda;
						llegidaClau = true;
					} else {
						llegidaClau = false;
						switch (comandaLLegida) {
						case "ciutatId":
							if (!ciutatId.equals("0")) {

								Ciutat_Dades cd = new Ciutat_Dades(Integer
										.parseInt(ciutatId), ciutatNom, Integer
										.parseInt(ciutatCoordX), Integer
										.parseInt(ciutatCoordY), objectius);
								
								
								ciutatLLista.add(cd);
								
								cd.setListObjectiuDades(objectius);
								
								objectius.clear();
							}
							ciutatId = comanda;
							break;
						case "ciutatNom":
							ciutatNom = comanda;
							break;

						case "ciutatCoordX":
							ciutatCoordX = comanda;
							break;

						case "ciutatCoordY":
							ciutatCoordY = comanda;
							break;

						case "objectiuId":
							objectiuId = comanda;
							break;

						case "objectiuTipus":
							objectiuTipus = comanda;
							break;
						case "objectiuSubTipusId":
							objectiuSubTipusId = comanda;
							break;
						case "objectiuCoordX":
							objectiuCoordX = comanda;
							break;
						case "objectiuCoordY":
							objectiuCoordY = comanda;
							break;
						case "objectiuDescripcio":

							objectiuDescripcio = comanda;
							
							objectius.add(new Objectiu_Dades(Integer
									.parseInt(objectiuId), true, Integer
									.parseInt(objectiuTipus), Integer
									.parseInt(objectiuSubTipusId), Integer
									.parseInt(objectiuCoordX), Integer
									.parseInt(objectiuCoordY),
									objectiuDescripcio));
							
							int cId = Integer.parseInt(ciutatId);
							id.put(cId,(ArrayList<Objectiu_Dades>) objectius);
							break;

						default:
							System.out.println(comanda);
						}
					}
				}
			}

			ciutatLLista.add(new Ciutat_Dades(Integer.parseInt(ciutatId),
					ciutatNom, Integer.parseInt(ciutatCoordX), Integer
							.parseInt(ciutatCoordY), objectius));
			objectius.clear();

		} catch (FileNotFoundException e) {
			System.out.println("Fitxer no existeix");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (fr != null)
				fr.close();
			System.out.println("menu11(): FINAL");
			// System.out.println();
		}
	}

	// 12. Carregar en la BD les ciutats i els seus objectiu
	void menu12(ArrayList<Ciutat_Dades> ciutatLLista) {
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");

		try {
			for (Ciutat_Dades dadaTmp : ciutatLLista) {

				
				
				ObjectSet<Ciutat_Dades> result = db.queryByExample(dadaTmp);
				if (!result.hasNext()) {
					db.store(dadaTmp);
				}
				// Aquí, abans de fer el store, hauriem de comprobar si el
				// subtipus ja existeix
				// en la BD i si ja existeix fer un update i no pas un insert.
			}
		} finally {
			db.close(); // Fem el bloc try-finally per asegurar-nos el tancament
						// de la DB.
			System.out.println("menu12(): FINAL");
			// System.out.println();
		}
	}
  //21. LListar les ciutats
    void menu21() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");

        try { 
            Predicate<Ciutat_Dades> p = new Predicate<Ciutat_Dades>() { 
                /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override 
                public boolean match(Ciutat_Dades c) { 
                    return true; 
                } 
            }; 
            
            List<Ciutat_Dades> result = db.query(p); 
            
            for (Ciutat_Dades dadaTmp : result) { 
               System.out.println(dadaTmp.getListObjectiuDades()); 
            } 
        } finally { 
            db.close(); 
            System.out.println("menu21(): FINAL");
        }         
    }
	
}