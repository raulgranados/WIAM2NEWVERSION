package practica;

import java.sql.Date;

public class Missatge_Dades {
    int id;			//L'usuari no pot posar aquest nº, ho ha de fer el sistema i
				//si ha de posar un id nou mirarà en la BD quin és el més gran i li sumarà 1.
    int transmissorId;		//Serà la constant idDeMiRadio declarada en la classe Radio_R832.
    String IPOrigen;		//Serà la constant IPDeMiRadio declarada en la classe Radio_R832.
    String missatge;		//El text del missatge que s'envia/rep.
    Date dataEnviament;		//La data d'avui,o sigui: new Date().
    Date dataRecepcio;		//La data d'avui,o sigui: new Date().

    
    public Missatge_Dades(int id, int transmissorId, String IPOrigen, String missatge, Date dataEnviament, Date dataRecepcio) {
        this.id = id;
        this.transmissorId = transmissorId;
        this.IPOrigen = IPOrigen;
        this.missatge = missatge;
        this.dataEnviament = dataEnviament;
        this.dataRecepcio = dataRecepcio;
    }

    
    public int getId() {
        return id;
    }

    public int getTransmissorId() {
        return transmissorId;
    }

    public String getIPOrigen() {
        return IPOrigen;
    }

    public String getMissatge() {
        return missatge;
    }

    public Date getDataEnviament() {
        return dataEnviament;
    }

    public Date getDataRecepcio() {
        return dataRecepcio;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTransmissorId(int transmissorId) {
        this.transmissorId = transmissorId;
    }

    public void setIPOrigen(String IPOrigen) {
        this.IPOrigen = IPOrigen;
    }

    public void setMissatge(String missatge) {
        this.missatge = missatge;
    }

    public void setDataEnviament(Date dataEnviament) {
        this.dataEnviament = dataEnviament;
    }

    public void setDataRecepcio(Date dataRecepcio) {
        this.dataRecepcio = dataRecepcio;
    }
    
    
    
    
}