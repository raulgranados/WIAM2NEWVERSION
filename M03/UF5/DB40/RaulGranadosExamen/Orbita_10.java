package practica;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;

import db40o.Client;
import db40o.Encarrec;
import db40o.Producte;

public class Orbita_10 {
	
	// ***** METODO CONTINUAR CON EL MENU ***** //
	
    public static void bloquejarPantalla() {
    	
    	// declaramos un objeto tipo Scanner para leer de teclado
    	
        Scanner in = new Scanner(System.in);
        System.out.print("Toca 'C' per a continuar ");
        
        // en caso de introducir una 'c', abandona la aplicación
        
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }
    
    
    // ***** MAIN ***** //
    
    public static void main(String[] args) throws IOException {
    	

    	
        String opcio;
        Scanner sc = new Scanner(System.in);
        
        LinkedList<SubTipus_Dades> subTipusLLista = new LinkedList<>(); //  lista doblemente enlazada
        SubTipus subTipusTmp = new SubTipus();
        
        ArrayList<Ciutat_Dades> ciutatLLista = new ArrayList<>();
        LinkedList<Objectiu_Dades> objectiuLLista = new LinkedList<>();
        Ciutat ciutat = new Ciutat();
        Objectiu objectiu = new Objectiu();
        
        Radio_R832 radio = new Radio_R832();
        
       // Streams streams = new Streams();
        
        do {
            System.out.println();
            System.out.println("0. RADIO");
            System.out.println("1. Carregar en memòria el fitxer de subTipus");
            System.out.println("2. Carregar en la BD els subTipus carregats en memòria");
            System.out.println("3. LListar els subTipus disponibles de la BD");
            System.out.println("4. Afegir un subTipus a la BD");
            System.out.println("5. Modificar un subTipus de la BD");
            System.out.println();
            System.out.println("11. Carregar en memòria el fitxer de ciutats vs objectiu");
            System.out.println("12. Carregar en la BD les ciutats i els seus objectiu");
            System.out.println();
            System.out.println("21. LListar les ciutats");
            System.out.println("22. LListar les ciutats i els seus objectius");
            System.out.println("23. LListar 1 ciutat i els seus objectius");
            System.out.println();
            System.out.println("31. Afegir un objectiu");
            System.out.println("32. Modificar un objectiu");
            System.out.println("33. Anular un objectiu");
            System.out.println();
            System.out.println("41. Fer una copia de seguretat de la BD");
            System.out.println("42. Esborrar tota la BD");
            System.out.println();
            System.out.println("50. Tancar el programa");
            System.out.println();
            System.out.print("opció?: ");
            opcio = sc.next();
            
            switch (opcio) {
            	case "0":
            		radio.mostrarMenu();
            		 bloquejarPantalla();
                     break;
                case "1":
                    subTipusTmp.menu1(subTipusLLista);
                    bloquejarPantalla();
                    break;
                case "2":
                    subTipusTmp.menu2(subTipusLLista);
                    bloquejarPantalla();
                    break;
                case "3":
                    subTipusTmp.menu3();
                    bloquejarPantalla();
                    break;
	                case "4":
	                subTipusTmp.menu4();
                    bloquejarPantalla();
                    break;
                case "5":
                	subTipusTmp.menu5();
                    bloquejarPantalla();
                    break;
                case "11":
                	ciutat.menu11(ciutatLLista);
                    bloquejarPantalla();
                    break;  
                case "12":
                	ciutat.menu12(ciutatLLista);
                    bloquejarPantalla();
                    break; 
                case "21":
                	ciutat.menu21();
                    bloquejarPantalla();
                    break;     
                case "50":
                    break; 
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }   
            
            
            
        } while (!opcio.equals("50"));
    }
    
}
