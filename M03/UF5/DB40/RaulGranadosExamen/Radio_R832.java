package practica;

import java.sql.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import db40o.Client;

public class Radio_R832 {
	int idDeMiRadio = 1;
	String IPDeMiRadio = "BARCELONA";

	
	List<Conversa_Dades> llistaConverses=new LinkedList<>();
	void mostrarMenu(){
	
        String opcio;
        Scanner sc = new Scanner(System.in);
            
        do {
        	
		System.out.println("1.LListar totes les converses");
		System.out.println("2.LListar tots el missatges");
		System.out.println("3.Enviar un missatge");
		System.out.println("4.Anul·lar una conversa");
		System.out.println("5.Anular totes les converses");
		System.out.println("6. Esborrar de la BD totes les converses");
		System.out.println("50. Tancar el menu de la radio");
		
		System.out.println();
        System.out.print("opció?: ");
        opcio = sc.next();
                
        switch (opcio) {
        	case "1":
        		menu1(llistaConverses);
        		Orbita_10.bloquejarPantalla();
        		break;
        	case "2":	
        		menu2();
        		Orbita_10.bloquejarPantalla();
                break; 	
        	case "3":	
        		menu3(llistaConverses);
        		Orbita_10.bloquejarPantalla();
                break;
        	case "4":	
        		menu4();
        		Orbita_10.bloquejarPantalla();
                break;   
        	case "5":	
        		menu5();
        		Orbita_10.bloquejarPantalla();
                break;     
              default:
            	  System.out.println("COMANDA NO RECONEGUDA");
            	  
        }} while (!opcio.equals("50"));
	}
        
	void menu3(List<Conversa_Dades> llistaConverses) {
			
		String novaConversa;
		Scanner sc = new Scanner(System.in);
		ObjectContainer db = Db4oEmbedded.openFile("BD-exercici.db4o");
		
		try { 
			Predicate p = new Predicate<Conversa_Dades>() { 
				@Override 
				public boolean match(Conversa_Dades c) { 
					return true; 
				} 
			};
		
		List<Conversa_Dades> result = db.query(p);
		
		for (Conversa_Dades dadaTmp : result) {	
			System.out.println("CONVERSES: "+dadaTmp.getId()+" "+dadaTmp.getDescripcio()+" "
		+dadaTmp.activa);
	}
		
			System.out.println("Per a crear una nova conversa (N)");
			System.out.println("Per afegir un missatge (A)");
			novaConversa = sc.next().toLowerCase();
			if (novaConversa.equals("n")) {
			int idConversa = llistaConverses.get(llistaConverses.size()).getId()+1;
			String descripcioConversa = "conversa num " + (idConversa+1);
			boolean estado = false;
			Conversa_Dades cd = new Conversa_Dades((idConversa), descripcioConversa, estado);
			llistaConverses.add(cd);
			db.store(cd);
			}else {
		

		System.out.println("A quina conversa vol assignar el missatge (ID)?: ");
		String c = sc.next();
		System.out.println("Escrigui el missatge que vol afegir :");
		String missatge = sc.next();
		int idSubTipo =  result.get(result.size()-1).getId()+1;
		
		int missatgeId = (llistaConverses.get((Integer.parseInt(c)-1)).getLlistaMissatges().size());
		llistaConverses.get((Integer.parseInt(c)-1)).getLlistaMissatges()
		.add(new Missatge_Dades((missatgeId+1), idDeMiRadio, IPDeMiRadio, missatge, new Date(2015, 11, 20), new Date(2015, 11, 20)));
		
			}
		
		} finally {
			db.close();
			System.out.println("menu3(): FINAL");
		}
	}
	
	void menu1(List<Conversa_Dades> llistaConverses) {
		
		ObjectContainer db = Db4oEmbedded.openFile("BD-exercici.db4o");
		
		try { 
			Predicate p = new Predicate<Conversa_Dades>() { 
				@Override 
				public boolean match(Conversa_Dades c) { 
					return true; 
				} 
			};
		
			List<Conversa_Dades> result = db.query(p);
			System.out.println(result.size());
			for (Conversa_Dades dadaTmp : result) {
				
				System.out.println("CONVERSES: "+dadaTmp.getId()+" "+dadaTmp.getDescripcio()+" "
			+dadaTmp.activa+" "+dadaTmp.llistaMissatges.toString());
			}
		} finally { 
			db.close(); 
			System.out.println("menu1(): FINAL");
			//System.out.println();
		}		
	
	}
	
	void menu2(){
		
		
	}
	void menu4(){
		
		ObjectContainer db = Db4oEmbedded.openFile("BD-exercici.db4o");
		
		String c;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quina conversa vol anul·lar (ID)? ");
		c = sc.next();
		int id = Integer.parseInt(c);
		Conversa_Dades conversa = new Conversa_Dades (id,null,(Boolean) null);
		
		ObjectSet<Conversa_Dades>esborraConversa = db.queryByExample(conversa);
		
		Conversa_Dades cd = esborraConversa.get(0);
		db.delete(cd);

	}
	
	void menu5(){
		
		ObjectContainer db = Db4oEmbedded.openFile("BD-exercici.db4o");
		Conversa_Dades conversa = new Conversa_Dades (0,null,(Boolean) null);
		
		ObjectSet<Conversa_Dades>esborraConversa = db.queryByExample(conversa);
		
		while (esborraConversa.hasNext()){
			Conversa_Dades cd = esborraConversa.next();
			db.delete(cd);
		}

	}
	       
}



