package practica;

import java.util.LinkedList;
import java.util.List;

public class Conversa_Dades {
    int id;			//L'usuari no pot posar aquest nº, ho ha de fer el sistema i
				//si ha de posar un id nou mirarà en la BD quin és el més gran i li sumarà 1.
    String descripcio;		//La descripció o nom que tindrà la conversa perquè l'usuari la diferencii de les altres.
    boolean activa;		//Per defecte estarà a true.
    
    List<Missatge_Dades> llistaMissatges = new LinkedList<Missatge_Dades>();

    
    public Conversa_Dades(int id, String descripcio, boolean activa) {
        this.id = id;
        this.descripcio = descripcio;
        this.activa = activa;
        llistaMissatges.addAll(llistaMissatges);
    }

    
    public int getId() {
        return id;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public boolean isActiva() {
        return activa;
    }

    public List<Missatge_Dades> getLlistaMissatges() {
        return llistaMissatges;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public void setLlistaMissatges(List<Missatge_Dades> llistaMissatges) {
        this.llistaMissatges = llistaMissatges;
    }
    
    public void sumaMissatge(Missatge_Dades m) {
        llistaMissatges.add(m);
    }
    public void llistaDeMissatgesToString(){
    	for (Missatge_Dades missatge_Dades : llistaMissatges) {
    		System.out.println(missatge_Dades.toString());
			
		}
    }
}
