package exerciciDB4O_2;

import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class Radio_R832_modulXifratge {
	
	static SecretKey secretKey = null;

	static SecretKey menu1(int keySize) {
		SecretKey sKey = null; 
		    if ((keySize == 128)||(keySize == 192)||(keySize == 256)) { 
		        try { 
		            KeyGenerator kgen = KeyGenerator.getInstance("AES"); 
		            kgen.init(keySize); 
		            sKey = kgen.generateKey(); 
		        } catch (NoSuchAlgorithmException ex) { 
		            System.err.println("Generador no disponible."); 
		        } 
		    } 

		    return sKey; 
		}	
	

	//Definició d’un IV estàtic. Per l’AES ha der ser de 16 bytes (un bloc) 
	public static final byte[] IV_PARAM = {0x00, 0x01, 0x02, 0x03, 
		                                       0x04, 0x05, 0x06, 0x07, 
		                                       0x08, 0x09, 0x0A, 0x0B, 
		                                       0x0C, 0x0D, 0x0E, 0x0F}; 

	//encryptDataCBC
	public static byte[] menu2(SecretKey sKey) {
			String string = "Muyayo";
		    byte[] encryptedData = null; 
		    byte [] data = string.getBytes();
		    try { 
		        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); 
		        IvParameterSpec iv = new IvParameterSpec(IV_PARAM); 
		        cipher.init(Cipher.ENCRYPT_MODE, sKey, iv); 
		        encryptedData = cipher.doFinal(data); 
		    } catch (Exception ex) { 
		        System.err.println("Error xifrant les dades: " + ex); 
		    } 

		    return encryptedData; 
	} 
    
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("Toca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }
	
	public static void menu() {
		String opcio;
        Scanner sc = new Scanner(System.in);
        
        do {
            System.out.println();
            System.out.println("-----------RADIO R832 - MODUL DE XIFRATGE ------------");
            System.out.println("\tENCRIPTACIÓ SIMÈTRICA (AES 128 bits)");
            System.out.println("1. Generar clau simètrica");
            System.out.println("2. Encriptar cadena de text amb AES de 128 bits");
            System.out.println("3. Desencriptar cadena de text amb AES de 128 bits");
            System.out.println();
            System.out.println("\tENCRIPTACIÓ ASIMÈTRICA (RSA amb clau embolcallada)");
            System.out.println("11. Generar clau simètrica i público-privades");
            System.out.println("12. Encriptar cadena de text amb RSA amb clau embolcallada");
            System.out.println("13. Desencriptar cadena de text amb RSA amb clau embolcallada");
            System.out.println();
            System.out.println("50. Sortir del menú de la radio R832 - modul de xifratge");
            System.out.println();
            System.out.print("opció?: ");
            opcio = sc.next();
            
            switch (opcio) {
            	case "1":
            	System.out.println("Tamany key [128 | 192 | 256] : ");
            	int keySize = sc.nextInt();
            	secretKey = menu1(keySize);
            	System.out.println("La teva clau es: "+secretKey);
            	bloquejarPantalla();
            	break;
            	case "2":
            		System.out.println("TEXT : muayayo");
            		System.out.println(menu2(secretKey));
                	bloquejarPantalla();
                	break;
            	case "3":
                	System.out.println("nothing to do.");
                	bloquejarPantalla();
                	break;
            	case "11":
                	System.out.println("nothing to do.");
                	bloquejarPantalla();
                	break;
            	case "12":
                	System.out.println("nothing to do.");
                	bloquejarPantalla();
                	break;
            	case "13":
                	System.out.println("nothing to do.");
                	bloquejarPantalla();
                	break;
                case "50":
                    break; 
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }   
        } while (!opcio.equals("50"));
	}
	
}
