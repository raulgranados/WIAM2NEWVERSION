package exerciciDB4O_2;

import java.util.ArrayList;
import java.util.List;


/*
 * 	int id*			//ID de la ciutat, ha d'èsser únic
	String nom		
	int coordX
	int coordY
	List<Objectiu_Dades>
*/

public class Ciutat_Dades {

	private int id;
	private String nom;
	private int coordX;
	private int coordY;
	ArrayList<Objectiu_Dades> listObjectiuDades = new ArrayList<Objectiu_Dades>();
	
	public Ciutat_Dades() {

	}

	
	public Ciutat_Dades(int id, String nom, int coordX, int coordY,ArrayList<Objectiu_Dades> listObjectiuDades) {
		this.id = id;
		this.nom = nom;
		this.coordX = coordX;
		this.coordY = coordY;
		this.listObjectiuDades.addAll(listObjectiuDades);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getCoordX() {
		return coordX;
	}
	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}
	public int getCoordY() {
		return coordY;
	}
	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}
	public ArrayList<Objectiu_Dades> getListObjectiuDades() {
		return listObjectiuDades;
	}
	public void setListObjectiuDades(ArrayList<Objectiu_Dades> listObjectiuDades) {
		this.listObjectiuDades = listObjectiuDades;
	}

	@Override
	public String toString() {
		return "Ciutat_Dades [id=" + id + ", nom=" + nom + ", coordX=" + coordX
				+ ", coordY=" + coordY + ", listObjectiuDades="
				+ getListObjectiuDades() + "]";
	}
	
	
	
	
}
