/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciDB4O_2;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

/**
 *
 * @author gmartinez
 */
public class Radio_R832 {
    static final int idDeMiRadio = 1;
    static final String IPDeMiRadio = "88.43.57.186";
    
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("Toca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }
    
    //1. LListar totes les converses
    private static void menu1() {
    	ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
    	try {
    	Predicate<Conversa_Dades> p = new Predicate<Conversa_Dades>() { 

			private static final long serialVersionUID = 1L;

			@Override 
            public boolean match(Conversa_Dades c) { 
                return true; 
            } 
        }; 
        
        List<Conversa_Dades> result = db.query(p); 
        
        if (result.size()>0) {
        
	        for(Conversa_Dades cd : result){
	        	System.out.println(cd);
	        	List<Missatge_Dades> missatges = cd.getLlistaMissatges();
	        	Iterator<Missatge_Dades> it = missatges.iterator();
	        	while(it.hasNext()){
	        		System.out.println(it.next());
	        	}
	        }
        
        } else  {
        	System.out.println("No hi a cap conversa.");
        }
	        
    	} finally {
    		System.out.println("Final MENU R.1");
    		db.close();
    	}
        
    }
    
    
    //2. LListar tots els missatges
    private static void menu2() {
    	ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
    	try {
    	Predicate<Missatge_Dades> p = new Predicate<Missatge_Dades>() { 

			private static final long serialVersionUID = 1L;

			@Override 
            public boolean match(Missatge_Dades m) { 
                return true; 
            } 
        }; 
        
        List<Missatge_Dades> result = db.query(p); 
        
        if (result.size()>0) {
        
        Iterator<Missatge_Dades> it = result.iterator();
        while (it.hasNext()) {
        	System.out.println(it.next());
        }
        

        } else  {
        	System.out.println("No hi a cap missatge.");
        }
        
    	}finally{
    		System.out.println("Final MENU R.2");
    		db.close();
    	}
        
    }
    
    
    private static int trobarNouIDMissatge(ObjectContainer db) {
    	int missatgeIdMesGran = 0;
    	Predicate<Missatge_Dades> p = new Predicate<Missatge_Dades>() { 

			private static final long serialVersionUID = 1L;

			@Override 
            public boolean match(Missatge_Dades m) { 
                return true; 
            } 
        }; 
        
        List<Missatge_Dades> result = db.query(p); 
               
        if (result.size()>0) {
        	missatgeIdMesGran = result.get(result.size()-1).getId()+1;
        } else {
        	return 1;
        }              
        
        return missatgeIdMesGran;
    }
    
    
    private static int trobarNouIDconversa(ObjectContainer db) {
    	int conversaIdMesGran = 0;
    	Predicate<Conversa_Dades> p = new Predicate<Conversa_Dades>() { 

			private static final long serialVersionUID = 1L;

			@Override 
            public boolean match(Conversa_Dades c) { 
                return true; 
            } 
        }; 
        
        List<Conversa_Dades> result = db.query(p); 
        
        for(Conversa_Dades cd : result){
        	System.out.println(cd);
        }
        
        if (result.size()>0) {
        	conversaIdMesGran = result.get(result.size()-1).getId()+1;
        } else {
        	System.out.println("No hi a cap conversa");
        	return 1;
        }              
        
        return conversaIdMesGran;
    }    
    
    
    
    //3. Enviar 1 missatge
    //El missatge s'ha d'assignar a una conversa que existeixi o a una nova (que ha llavors es crearà aquí)
    private static void menu3() {

    	ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
    	Scanner sc = new Scanner(System.in);
    	
    	try {
    		
    		int idConversa = trobarNouIDconversa(db);
    		int idMissatge = trobarNouIDMissatge(db);
    		
    		List<Conversa_Dades> listConversas = Radio_R832_Dades.getLlistaConverses();

    		System.out.println("Per a crear una nova conversa (N)");
        	System.out.println("Sel·lecció: ");
       	
        	
        	String select = sc.next();
        	
        	if (select.equalsIgnoreCase("N")) {
        		System.out.println("Creació nova conversa\n================\n");
	        	System.out.println("[NO SE PUEDEN INTRODUCIR ESPACIOS] Introdueix descripcio: ");
	        	String saltoErrorLinea = sc.nextLine();
	        	String descripcio = sc.nextLine().trim();
				System.out.println("Introdueix activa [TRUE or FALSE] : ");
				boolean activa = sc.nextBoolean();
				Conversa_Dades conversa = new Conversa_Dades(idConversa,descripcio, activa);
				listConversas.add(conversa);
				db.store(Radio_R832_Dades.getLlistaConverses());
				List<Missatge_Dades> listMissatges = conversa.getLlistaMissatges();
    			System.out.println("Introdueix un missatge: ");
    			String missatge = sc.next();
    			listMissatges.add(new Missatge_Dades(idMissatge, idDeMiRadio, IPDeMiRadio, missatge, new Date(),new Date()));
    			db.store(conversa.getLlistaMissatges());
        	} else {
        		int selectID = Integer.parseInt(select);
        		Conversa_Dades conversa = new Conversa_Dades();
        		conversa.setId(selectID);
        		ObjectSet<Conversa_Dades> conversaDades = db.queryByExample(conversa);
        		if (conversaDades.hasNext()) {
        			Conversa_Dades cdAux = conversaDades.next();
        			List<Missatge_Dades> listMissatges = cdAux.getLlistaMissatges();
        			System.out.println("Introdueix un missatge: ");
        			String missatge = sc.next();
        			listMissatges.add(new Missatge_Dades(idMissatge, idDeMiRadio, IPDeMiRadio, missatge, new Date(),new Date()));
        			db.store(cdAux.getLlistaMissatges());
        		}
        	}
        	
    	} finally {
    		System.out.println("Final MENU R.3");
    		db.close();
    	}
    }
    
    private static void menu4() {
    	
    	menu1();
    	
    	ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
    	Scanner sc = new Scanner(System.in);
    	try {
    	System.out.println("Selecciona una ID: ");
    	int idConversa = sc.nextInt();
        Conversa_Dades c = new Conversa_Dades();
        c.setId(idConversa);
        ObjectSet<Conversa_Dades> rConversa = db.queryByExample(c);        
        Conversa_Dades aux = rConversa.next();
        aux.setActiva(false);
        db.store(aux);
        
    	} finally {
    		System.out.println("Final MENU R.4");
    		db.close();
    	}
    }
    
    //5. Anular totes les converses
    //Posar "activa" = false
    private static void menu5() {
    	ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
    	
    	try {
    	
    	Predicate<Conversa_Dades> p = new Predicate<Conversa_Dades>() { 

			private static final long serialVersionUID = 1L;

			@Override 
            public boolean match(Conversa_Dades c) { 
                return true; 
            } 
        }; 
        
        List<Conversa_Dades> result = db.query(p); 
        
        for (Conversa_Dades c : result) {
        	c.setActiva(false);
        	db.store(c);
        }
        
    	} finally {
    		System.out.println("Final MENU R.5");
    		db.close();
    	}
        
    }
    
    
    //6. Esborrar de la BD totes les converses
    private static void menu6() {
    	ObjectContainer db = Db4oEmbedded.openFile("baseDeDades"+File.separator+"BD-exercici.db4o");
    	try {
    		Predicate<Conversa_Dades> p = new Predicate<Conversa_Dades>() { 

    			private static final long serialVersionUID = 1L;

    			@Override 
                public boolean match(Conversa_Dades c) { 
                    return true; 
                } 
            }; 
            
            List<Conversa_Dades> result = db.query(p); 
            
            for (Conversa_Dades c : result) {
            	db.delete(c);
            	System.out.println("Eliminada "+c);
            }
            
            Predicate<Missatge_Dades> md = new Predicate<Missatge_Dades>() { 

    			private static final long serialVersionUID = 1L;

    			@Override 
                public boolean match(Missatge_Dades m) { 
                    return true; 
                } 
            }; 
            
            List<Missatge_Dades> resultMissatges = db.query(md); 
            for (Missatge_Dades m : resultMissatges) {
            	db.delete(m);
            	System.out.println("Eliminado "+m);
            }
            
    	} finally {
    		System.out.println("Final MENU R.6");
    		db.close();
    		}
    }
    
    
   
    
    static void menuPrincipalRadio() {
        String opcio;
        Scanner sc = new Scanner(System.in);
        
        do {
            System.out.println();
            System.out.println("1. LListar totes les converses");
            System.out.println("2. LListar tots els missatges");
            System.out.println("3. Enviar 1 missatge");
            System.out.println("4. Anular 1 conversa");
            System.out.println("5. Anular totes les converses");
            System.out.println("6. Esborrar de la BD totes les converses");
            System.out.println();
            System.out.println("50. Tancar el menú de la radio");
            System.out.println();
            System.out.print("opció?: ");
            opcio = sc.next();
            
            switch (opcio) {
                case "1":
                    menu1();
                    bloquejarPantalla();
                    break;
                case "2":
                    menu2();
                    bloquejarPantalla();
                    break;
                case "3":
                    menu3();
                    bloquejarPantalla();
                    break;
                case "4":
                    menu4();
                    bloquejarPantalla();
                    break;
                case "5":
                    menu5();
                    bloquejarPantalla();
                    break;
                case "6":
                    menu6();
                    bloquejarPantalla();
                    break;
                case "50":
                    break; 
                default:
                    System.out.println("COMANDA NO RECONEGUDA");
            }   
        } while (!opcio.equals("50"));
    }

    
}
