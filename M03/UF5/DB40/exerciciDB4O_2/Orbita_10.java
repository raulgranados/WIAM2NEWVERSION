package exerciciDB4O_2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Orbita_10 {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("Toca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }
    
	public static void main(String[] args) {
	        String opcio;
	        Scanner sc = new Scanner(System.in);
	        
	        LinkedList<SubTipus_Dades> subTipusLLista = new LinkedList<>();
	        SubTipus subTipusTmp = new SubTipus();
	        
	        ArrayList<Ciutat_Dades> ciutatLLista = new ArrayList<>();
	        LinkedList<Objectiu_Dades> objectiuLLista = new LinkedList<>();
	        
	        Ciutat ciutat = new Ciutat();
	        Ciutat2 ciutat2 = new Ciutat2();
	        
	        Bd bd = new Bd();
	        
	        Objectiu objectiu = new Objectiu();
	        
	        // Streams streams = new Streams();
	        
	        do {
	            System.out.println();
	            System.out.println("1. Carregar en memòria el fitxer de subTipus");
	            System.out.println("2. Carregar en la BD els subTipus carregats en memòria");
	            System.out.println("3. LListar els subTipus disponibles de la BD");
	            System.out.println("4. Afegir un subTipus a la BD");
	            System.out.println("5. Modificar un subTipus de la BD");
	            System.out.println();
	            System.out.println("11. Carregar en memòria el fitxer de ciutats vs objectiu");
	            System.out.println("12. Carregar en la BD les ciutats i els seus objectiu");
	            System.out.println();
	            System.out.println("21. LListar les ciutats");
	            System.out.println("22. LListar les ciutats i els seus objectius");
	            System.out.println("23. LListar 1 ciutat i els seus objectius");
	            System.out.println("24. Afegir una ciutat");
	            System.out.println("25. Modificar una ciutat");
	            System.out.println("26. Copiar els objectius d'una ciutat a una altre ciutat");
	            System.out.println("27. Llistar les ciutats segons els valor total dels seus objectius");
	            System.out.println("28. Eliminar una ciutat");
	            System.out.println();
	            System.out.println("31. Afegir un objectiu");
	            System.out.println("32. Modificar un objectiu");
	            System.out.println("33. Anular un objectiu");
	            System.out.println("34. LListar tots els objectius ordenats per id");
	            System.out.println();
	            System.out.println("41. Fer una copia de seguretat de la BD");
	            System.out.println("42. Esborrar tota la BD");
	            System.out.println();
	            System.out.println("R. Accedir a la radio VHF/HF R-832 Evkalipt");
	            System.out.println();
	            System.out.println("50. Tancar el programa");
	            System.out.println();
	            System.out.print("opció?: ");
	            opcio = sc.next();
	            
	            switch (opcio) {
	                case "1":
					try {
						subTipusTmp.menu1(subTipusLLista);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	                    bloquejarPantalla();
	                    break;
	                case "2":
	                	subTipusTmp.menu2(subTipusLLista);
	                    bloquejarPantalla();
	                    break;
	                case "3":
	                	subTipusTmp.menu3();
	                    bloquejarPantalla();
	                    break;
	                case "4":
	                	subTipusTmp.menu4();
	                    bloquejarPantalla();
	                    break;
	                case "5":
	                	subTipusTmp.menu5();
	                    bloquejarPantalla();
	                    break;
	                case "11":
						ciutat2.menu11(ciutatLLista);
						bloquejarPantalla();
						break;
                    
	                case "12":
	                	ciutat2.menu12(ciutatLLista);
	                    bloquejarPantalla();
	                	break;
	                	
	                case "21":
	                	ciutat2.menu21();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "22":
	                	ciutat2.menu22();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "23":
	                	ciutat2.menu23();
	                	bloquejarPantalla();
	                	break;
	                
	                case "24":
	                	ciutat2.menu24();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "25":
	                	ciutat2.menu25();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "26":
	                	ciutat2.menu26();
	                	bloquejarPantalla();
	                	break;
	                
	                case "27":
	                	ciutat2.menu27(ciutatLLista);
	                	bloquejarPantalla();
	                	break;
	                	
	                case "28":
	                	ciutat2.menu28();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "31":
	                	objectiu.menu31();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "32":
	                	objectiu.menu32();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "33":
	                	objectiu.menu33();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "34":
	                	objectiu.menu34();
	                	bloquejarPantalla();
	                	break;
	                	
	                case "41":
					try {
						bd.menu41();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    				
	    				bloquejarPantalla();
	    				break;
	    				
	    			case "42":
	    				bd.menu42();
					try {
						ciutat.menu11(ciutatLLista);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    				ciutat.menu12(ciutatLLista);
					try {
						subTipusTmp.menu1(subTipusLLista);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    				subTipusTmp.menu2(subTipusLLista);
	    				bloquejarPantalla();
	    				break;
	                	
	    			case "R": Radio_R832.menuPrincipalRadio();
	    				break;
	    			
	    			case "r": Radio_R832.menuPrincipalRadio();
    				break;
	    				
	                case "50":
	                    break; 
	                default:
	                    System.out.println("COMANDA NO RECONEGUDA");
	            }   
	            
	            
	            
	        } while (!opcio.equals("50"));
	    }
	}
