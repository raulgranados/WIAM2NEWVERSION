/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciDB4O_2;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;



/**
 *
 * @author gmartinez
 */
public class SubTipus {
   
    //1. Carregar en memòria el fitxer de subTipus
    void menu1(LinkedList<SubTipus_Dades> subTipusLLista) throws IOException {
        File fitxer = new File("arxiusPerCarregar/subTipus.txt");     // Adreçament relatiu   
        FileReader fr = null;                     // Entrada
        BufferedReader br;                        // Buffer   
        String liniaLLegida;
        String comandes[];
        String comandaLLegida = "";
        boolean llegidaClau;
        String subTipusId = "0";
        String subTipusNom = "";
        String subTipusValor = "0";

        try {
            fr = new FileReader (fitxer);          // Inicialitza entrada del fitxer
            br = new BufferedReader(fr);           // Inicialitza buffer amb l’entrada
            while((liniaLLegida = br.readLine())!= null) {
                comandes = liniaLLegida.split("=");
                llegidaClau = false;
                for (String comanda : comandes) {
                    if (llegidaClau == false) {
                        comandaLLegida = comanda;
                        llegidaClau = true;
                    } else {
                        llegidaClau = false;
                        switch (comandaLLegida) {
                            case "subTipusId":
                                subTipusId = comanda;
                                break;
                            case "subTipusNom":
                                subTipusNom = comanda;
                                break;
                            case "subTipusValor":
                                subTipusValor = comanda;
                                //System.out.println("SUBTIPUS: " + subTipusId + " " + subTipusNom + " " + subTipusValor);
                                subTipusLLista.add(new SubTipus_Dades(Integer.parseInt(subTipusId), subTipusNom, Integer.parseInt(subTipusValor)));
                                break;
                            default:
                                System.out.println("COMANDA EN FITXER SUBTIPUS NO RECONEGUDA");
                        }                        
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Fitxer no existeix");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (fr != null) fr.close();
            System.out.println("menu1(): FINAL");
            //System.out.println();
        } 
    }
    
    
   //2. Carregar en la BD els subTipus carregats en memòria
   void menu2(LinkedList<SubTipus_Dades> subTipusLLista) {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BD-exercici.db4o");
        
        try {
            for (SubTipus_Dades dadaTmp: subTipusLLista) {
            	
            	ObjectSet<SubTipus_Dades> result = db.queryByExample(dadaTmp);
            	
            	if (result.hasNext()) {
            		
            		SubTipus_Dades dTmp = (SubTipus_Dades) result.next();
            		dTmp.setNom(dTmp.getNom());
            		dTmp.setValor(dTmp.getValor());
            		
            		System.out.println("Se ha actualizado SubTipus: " + dTmp.getId() +" "+ dTmp.getNom() +" "+ dTmp.getValor());
            		
            	} else {
            	
            		db.store(dadaTmp);
            		
            	}            	
                //Aquí, abans de fer el store, hauriem de comprobar si el subtipus ja existeix
                //en la BD i si ja existeix fer un update i no pas un insert.
            }
        } finally {
            db.close();     //Fem el bloc try-finally per asegurar-nos el tancament de la DB.
            System.out.println("menu2(): FINAL");
            //System.out.println();
        }
    }
    
    
    //3. LListar els subTipus disponibles de la BD
    void menu3() {
        ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BD-exercici.db4o");

        try { 
            Predicate<SubTipus_Dades> p = new Predicate<SubTipus_Dades>() { 
                /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override 
                public boolean match(SubTipus_Dades c) { 
                    return true; 
                } 
            }; 
            
            List<SubTipus_Dades> result = db.query(p); 
            
            for (SubTipus_Dades dadaTmp : result) { 
                System.out.println("SubTipus: " + dadaTmp.getId() + " " + dadaTmp.getNom() + " " + dadaTmp.getValor()); 
            } 
        } finally { 
            db.close(); 
            System.out.println("menu3(): FINAL");
        }         
    }

    /*  En el menú 4 hem de buscar l'últim Id que s'estigui fent servir en la BD 
     *  i sumar-li 1 (a llavors serà l'id que farem servir).
     *  S'ha de demanar a l'usuari el nom i el valor del subtipus i a llavors fer l'insert en la BD.*/
    
    static int getLastId(){
    	ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BD-exercici.db4o");
    	try { 
	 	Predicate<SubTipus_Dades> p = new Predicate<SubTipus_Dades>() { 
            /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override 
            public boolean match(SubTipus_Dades c) { 
                return true; 
            } 
        }; 
        
        List<SubTipus_Dades> result = db.query(p); 
        
        SubTipus_Dades lastId = result.get(result.size()-1);
        
        int id = lastId.getId()+1;
        
        return id;
        
    	}finally{
    		db.close();
    	}
    }
    
	void menu4() {
		 int id = getLastId();
		 Scanner sc = new Scanner(System.in);
		 ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BD-exercici.db4o");
		 try {             
	            System.out.print("Name: ");
	            String nom = sc.next();
	            System.out.println("Value: ");
	            int valor = sc.nextInt();
	            	            
	            LinkedList<SubTipus_Dades> subTipusLLista = new LinkedList<SubTipus_Dades>();
	            SubTipus_Dades nDada = new SubTipus_Dades(id,nom,valor);
	            if (subTipusLLista.add(nDada)) {
	            	db.close();
	            	menu2(subTipusLLista);
	            } 
	            
		 }finally{
			 db.close();
			 //sc.close();
			 System.out.println("menu4(): FINAL");
		 }
	}
	
	void menu5() {
		menu3();
		int id = getLastId();
		ObjectContainer db = Db4oEmbedded.openFile("baseDeDades/BD-exercici.db4o");
		try { 
			Scanner sc = new Scanner(System.in);
			System.out.print("Select ID: ");
			int select = sc.nextInt();
			if (select<=id) {
				System.out.println("Vamos a modificar "+select);
				SubTipus_Dades findSubD = new SubTipus_Dades(select, null, 0);
				ObjectSet<SubTipus_Dades> result = db.queryByExample(findSubD);
				if (result.hasNext()) {
					System.out.println("Introduce nuevo nombre: ");
					String nombre = sc.next();
					System.out.println("Introduce nuevo valor: ");
					int valor = sc.nextInt();
					
					SubTipus_Dades dTmp = (SubTipus_Dades) result.next();
            		dTmp.setNom(nombre);
            		dTmp.setValor(valor);
            		db.store(dTmp);
            		System.out.println("Se ha actualizado SubTipus: " + dTmp.getId() +" "+ dTmp.getNom() +" "+ dTmp.getValor());
					
				}
			} else {
				System.out.println("Error ... ");
			}
		} finally {
			db.close();
		}
	}
    
}


