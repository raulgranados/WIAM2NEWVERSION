package exerciciDB4O_2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class Bd {

	// 41. Fer una copia de seguretat de la BD
	void menu41() throws IOException {

		File dir = new File("backUpBD4O");
		if (!dir.exists()) {
			dir.mkdir();
		}

		FileInputStream in;
		FileOutputStream out;

		File bd = new File("baseDeDades"+File.separator+"BD-exercici.db4o");
		in = new FileInputStream(bd);

		File d = new File(dir.getPath()+ File.separator + "BD-exercici.db4o"
				+ new Date(bd.lastModified()));
		if (!d.exists()) {
			d.createNewFile();
			out = new FileOutputStream(d);
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			in.close();
			out.close();
		}

	}

	// 42. borrar bd
	void menu42() {
		File f = new File("baseDeDades"+File.separator+"BD-exercici.db4o");
		if (f.delete()) {
			System.out.println("BD borrada");
		} else {
			System.out.println("BD no encontrada");
		}
	}

}
