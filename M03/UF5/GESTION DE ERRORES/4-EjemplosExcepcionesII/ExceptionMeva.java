/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gmartinez
 */
public class ExceptionMeva extends Exception {
    public ExceptionMeva(String message) {
        super(message);
    }

    public String getLocalizedMessage() {
        String error = super.getMessage();
        error += "\nMissatge personalitzat";
        return error;
    }
}