
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gmartinez
 */


public class propagacioExcepcions {
    /*
    public static void prova0() { // Error
        //throw new Exception("prova0(): Texte de l’excepcio");
    }  
    
    public static void prova1() throws Exception { // Ok
        throw new Exception("PROVA1(): Texte de l’excepcio");
    }  

    public static void prova2() throws NullPointerException { // Ok
        throw new NullPointerException("PROVA2(): Texte de l’excepcio");
    }  

    public static void prova3() {  // Ok
        throw new NullPointerException("PROVA3(): Texte de l’excepcio");
    }  

    public static void main(String[] args) {
        prova0();
        
        try {
            prova1();
            //Excepcions que hereten de Exception (però no de RunTimeException): No 
            //és automàtic, cal propagar-les o controlar-les.
            //L'ús d'aquests mètodes obliga a controlar l'excepció: 
            //    Opció 1. Usant try/catch 
            //    Opció 2. Marcant el propi mètode com a throws 
        } catch (Exception ex) {
            System.out.println("PROVA1(): Catch: Error = " + ex.getMessage());
            System.out.println();
        } 

        prova2();

        prova3();
    }
    */
}
