package com.lau.exercici6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Exercici6 {

	public static void main(String[] args) {
		EntradaAgenda marc = new EntradaAgenda("Marc", "Garcia", "Sanchez");
		EntradaAgenda pep = new EntradaAgenda("Pep", "Valles", "Marquez");
		EntradaAgenda paula = new EntradaAgenda("Paula", "Gutierrez", "Pujol");
		EntradaAgenda mireia = new EntradaAgenda("Mireia", "Cases", "Esteve");
		
		List<EntradaAgenda> agenda = new ArrayList<EntradaAgenda>();
		agenda.add(marc);
		agenda.add(pep);
		agenda.add(paula);
		agenda.add(mireia);
		
		Collections.sort(agenda);
		
		Collections.sort(agenda, new Comparator<EntradaAgenda>() {

			@Override
			public int compare(EntradaAgenda o1, EntradaAgenda o2) {
				// TODO Auto-generated method stub
				return o1.getNom().compareTo(o2.getNom());
			}
			
		});
		
	}
}
