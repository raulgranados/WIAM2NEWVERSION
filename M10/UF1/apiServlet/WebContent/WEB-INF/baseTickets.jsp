<%@page import="java.util.ArrayList"%>
<%@page import="ticketsAndroid.Ticket"%>
<%@page import="ticketsAndroid.TicketServlet"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.HashMap"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%!
	List<Ticket> llista = new ArrayList<Ticket>();
	
	%>
<%
	llista=(List)request.getAttribute("tickets"); 
	%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista Tickets</title>
</head>
<body>
	
	<p style="font-size:20px"><b><u>Llista tickets: </u></b></p>
	<table border="1">
		<tr>
			<td><b><em>nº Ticket: </em></b></td>
			<td><b><em>Titulo: </em></b></td>
			<td><b><em>Autor: </em></b></td>
		</tr>
	<% for(int i=0; i<llista.size();i++){ %>
	
		<tr>
			<td><%=llista.get(i).getNum() %></td>
			<td><a href='TicketServlet?action=mostrar<%=llista.get(i).getNum() %>'><%=llista.get(i).getTitulo() %></a></td>
			<td><%=llista.get(i).getClient() %></td>
		</tr>
		
	
	<%} %>
</table>
	<br/>
	<a href='TicketServlet?action=crear'><input type='button' value='Crear ticket' /> </a>
	<a href='TicketServlet?action=logout'><input type='button' value='logout' /> </a>
</body>
</html>