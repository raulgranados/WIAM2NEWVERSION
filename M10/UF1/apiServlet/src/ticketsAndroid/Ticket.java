package ticketsAndroid;



import java.io.Serializable;

import javax.servlet.annotation.WebServlet;

import com.google.gson.annotations.SerializedName;
@WebServlet("/Ticket")
public class Ticket {
	
	@SerializedName ("num")
	int num;
	@SerializedName ("client")
	String client;
	@SerializedName ("titulo")
	String titulo;
	@SerializedName ("desc")
	String desc;
	transient Attachment at;
	
	public Ticket(){
		
	}
	
	public Ticket(int num, String client, String titulo, String desc, Attachment at) {
		super();
		this.num = num;
		this.client = client;
		this.titulo = titulo;
		this.desc = desc;
		this.at = at;
	}

	public Ticket(int num,String client, String titulo, String desc) {
		this.num=num;
		this.client = client;
		this.titulo = titulo;
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "Ticket [client=" + client + ", titulo=" + titulo + ", desc=" + desc + "]";
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public Attachment getAt() {
		return at;
	}

	public void setAt(Attachment at) {
		this.at = at;
	}
	
	
	
	
}
@WebServlet("/Attach")
class Attachment{
	
	
	String nom;
	byte[] contents;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public byte[] getContents() {
		return contents;
	}
	public void setContents(byte[] contents) {
		this.contents = contents;
	}

	
	
}