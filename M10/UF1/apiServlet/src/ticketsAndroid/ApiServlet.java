package ticketsAndroid;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
@WebServlet(urlPatterns = "/ApiServlet")
public class ApiServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		List<Ticket> tickets = TicketServlet.tickets;
		
		String json = new Gson().toJson(tickets);
//		String json = new Gson().toJson(TicketServlet.tickets.get(0));
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().println(json);
	}

	
	
	
}
