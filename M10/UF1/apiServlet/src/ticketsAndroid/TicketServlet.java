package ticketsAndroid;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;

@WebServlet(urlPatterns = "/TicketServlet", loadOnStartup = 1)
@MultipartConfig(location = "/tmp", fileSizeThreshold = 5_242_880, maxFileSize = 20_971_520L, maxRequestSize = 41_943_040L)
public class TicketServlet extends HttpServlet {

	TreeMap<Integer, User> users = new TreeMap<Integer, User>();

	static List<Ticket> tickets = new ArrayList<Ticket>();

	HttpSession session;

	public TicketServlet() throws IOException {
		tickets.add(new Ticket(1, "Joan", "Mail", "Correu Laura...",
				processAttachmentInici(new File("/home/users/inf/wiam2/iam46710930/Documents/M10/correuLaura"))));
		tickets.add(new Ticket(2, "Pere", "Info", "Informació",
				processAttachmentInici(new File("/home/users/inf/wiam2/iam46710930/Documents/M10/site.txt"))));
		tickets.add(new Ticket(3, "Pol", "UF4", "Tema 4",
				processAttachmentInici(new File("/home/users/inf/wiam2/iam46710930/Documents/M10/correuLaura"))));

		users.put(1, new User("pere", "123"));
		users.put(2, new User("pol", "456"));
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		session = req.getSession();

		

		if (session.getAttribute("user") == null) {
			if (!users.containsValue(new User(req.getParameter("user"), req.getParameter("pass")))) {

				req.getRequestDispatcher("/WEB-INF/log.jsp").forward(req, resp);
			} else {

				session.setAttribute("user", new User(req.getParameter("user"), req.getParameter("pass")));
				
			}
		}

		
		String key = req.getParameter("action");
		//envio de servlet a jsp ("nombre parametro", valor)
		req.setAttribute("tickets", tickets);

		
		if (key != null) {
			if (key.matches("mostrar[0-9]+")) {

				for (Ticket ti : tickets) {
					if (ti.getNum() == Integer.parseInt(key.replace("mostrar", ""))) {
						req.setAttribute("ticket", ti);
					}
				}

				req.getRequestDispatcher("/WEB-INF/ticket.jsp").forward(req, resp);
			}

			if (key.equals("crear")) {
				req.getRequestDispatcher("/WEB-INF/crearTicket.jsp").forward(req, resp);
			}
			if (key.equals("logout")) {
				session.setAttribute("user", null);
				doGet(req, resp);
			}

			if (key.matches("descargar[0-9]+")) {
				Ticket t = new Ticket();
				for (Ticket ti : tickets) {
					if (ti.getNum() == Integer.parseInt(key.replace("descargar", ""))) {
						t = ti;
					}
				}
				resp.setHeader("Content-Disposition", "attachment;filename=" + t.getAt().getNom());
				resp.setContentType("application/octet-stream");
				ServletOutputStream stream = resp.getOutputStream();
				stream.write(t.getAt().getContents());

				

			}
		} else {

			req.getRequestDispatcher("/WEB-INF/baseTickets.jsp").forward(req, resp);
		}
	
	}

	protected void  doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Part filePart = req.getPart("file");
//		if (!req.getParameter("nombre").trim().equals("") && !req.getParameter("titulo").trim().equals("")
//				&& !req.getParameter("desc").trim().equals("") && (filePart != null && filePart.getSize() > 0)) {
		if (!req.getParameter("titulo").trim().equals("")
				&& !req.getParameter("desc").trim().equals("") && (filePart != null && filePart.getSize() > 0)) {

			Attachment attach = new Attachment();
			attach = this.processAttachment(filePart);

//			tickets.add(new Ticket(tickets.size() + 1, req.getParameter("nombre"), req.getParameter("titulo"),
//					req.getParameter("desc"), attach));
			User usuari=(User)session.getAttribute("user");
			tickets.add(new Ticket(tickets.size() + 1,usuari.getUser() , req.getParameter("titulo"),
					req.getParameter("desc"), attach));
			doGet(req, resp);
		} else {
			System.out.println("Debes ntroducir todos los campos...");
			doGet(req, resp);
		}

	}

	private Attachment processAttachment(Part filePart) throws IOException {
		Attachment attach = new Attachment();

		InputStream inputStream = filePart.getInputStream();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		File f = new File("/tmp/" + filePart.getSubmittedFileName());

		BufferedOutputStream desti = new BufferedOutputStream(new FileOutputStream(f));

		int read = 0;

		byte[] b = new byte[1024];

		while ((read = inputStream.read(b)) != -1) {
			outputStream.write(b, 0, read);
			// desti.write(b,0,read);
		}
		outputStream.writeTo(desti);
		desti.close();
		attach.setNom(filePart.getSubmittedFileName());
		attach.setContents(outputStream.toByteArray());

		return attach;

	}

	private Attachment processAttachmentInici(File file) throws IOException {
		Attachment attach = new Attachment();

		BufferedReader in = new BufferedReader(new FileReader(file));

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		// File f = new File("/tmp/"+filePart.getSubmittedFileName());
		//
		// BufferedOutputStream desti = new BufferedOutputStream(new
		// FileOutputStream(f));

		int read = 0;

		byte[] b = new byte[1024];

		while ((read = in.read()) != -1) {
			outputStream.write(read);
			// desti.write(b,0,read);
		}

		// desti.close();
		attach.setNom(file.getName());
		attach.setContents(outputStream.toByteArray());

		return attach;

	}

}


