package com.lau;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*Exercici 6*/
@WebServlet(urlPatterns = { "/hello2", "/hello3"},
			name = "MyOwnServlet",
			description = "This is my first servlet")
public class Exemple0Servlet extends HttpServlet {
	
	private static int VISITANT=0;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
		Calendar cal = Calendar.getInstance();
		/* Exercici 1 */
		//resp.getWriter().println("hello world");
		/* Exercici 1.2 */
		//resp.getWriter().println("<h1>hello world</h1>");
		
		/* Exercici 2 */
		//resp.getWriter().println(cal.get(Calendar.DAY_OF_MONTH)+":"+cal.get(Calendar.MONTH)+":"+cal.get(Calendar.YEAR));
		
		/* Exercici 3 */
		VISITANT++;
		resp.getWriter().println("ets el visitant num "+VISITANT);
		/* Exercici 4 */
		//resp.sendRedirect("http://www.google.com");
		
	
	}
	/*Exercici 5*/
	@Override
	public void destroy() {
		System.out.println("Servlet " + this.getServletName() + " has stopped.");
	}
	@Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started.");
	}

}
